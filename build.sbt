import Dependencies._

ThisBuild / scalaVersion := "3.5.2"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / git.gitTagToVersionNumber := {
  tag: String =>
    if (tag matches "[0-9]+\\..*") Some(tag)
    else None
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "IIIF Manifest Creator",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / mainClass := Some("ch.memobase.App"),
    assembly / assemblyMergeStrategy := {
      case "log4j.properties"                           => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case "log4j2.xml"                                 => MergeStrategy.first
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    git.useGitDescribe := true,
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      dataModelExtraction,
      http4sDsl,
      http4sServer,
      iiifApisV2,
      iiifApisV3,
      kafkaStreams.cross(CrossVersion.for3Use2_13),
      log4jApi,
      log4jCore,
      log4jScala,
      log4jSlf4j,
      upickle,
      kafkaStreamsTestUtils % Test,
      scalatic % Test,
      scalaTest % Test
    )
  )
