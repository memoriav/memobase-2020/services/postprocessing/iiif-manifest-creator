# IIIF Manifest Creator

Creates a [IIIF manifest](https://iiif.io/api/presentation/2.1/) based on received metadata

## Mapping

The service extracts and maps certain values in the original metadata (which is in JSON-LD) to the IIIF manifest model. In order to perform this task, there are two main components at work:
* [_Extractors_](https://gitlab.switch.ch/memoriav/memobase-2020/services/postprocessing/iiif-manifest-creator/-/blob/master/src/main/scala/ch/memobase/Extractors.scala) for filtering the relevant values in the original metadata
* A [_manifest builder_](https://gitlab.switch.ch/memoriav/memobase-2020/services/postprocessing/iiif-manifest-creator/-/blob/master/src/main/scala/ch/memobase/Manifest.scala)

## Configuration

In order to work correctly, some environment variables have to be set:

* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of Kafka bootstrap server addresses
* `APPLICATION_ID`: Id used by Kafka Streams application (see [Kafka documentation](https://kafka.apache.org/documentation/#streamsconfigs_application.id) for details)
* `TOPIC_IN`: Name of Kafka topic where messages are read from
* `TOPIC_OUT`: Name of Kafka topic where messages are written to (without environment postfix)
* `TOPIC_PROCESS`: Name of Kafka topic where status reports are written to
* `REPORTING_STEP_NAME`: Determines the name of the step.
* `SECURITY_PROTOCOL: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
* `SSL_KEYSTORE_TYPE: The file format of the key store file. This is optional for client. 
* `SSL_KEYSTORE_LOCATION: The location of the key store file. This is optional for client and can be used for two-way authentication for client
* `SSL_KEYSTORE_KEY`: Private key in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with PKCS#8 keys. If the key is encrypted, key password must be specified using `SSL_KEY_PASSWORD`
* `SSL_KEYSTORE_CERTIFICATE_CHAIN`: Certificate chain in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with a list of X.509 certificates
* `SSL_TRUSTSTORE_TYPE: The file format of the trust store file. The values currently supported by the default `ssl.engine.factory.class` are `JKS`, `PKCS12` and `PEM`
* `SSL_TRUSTSTORE_LOCATION: The location of the trust store file
* `SSL_TRUSTSTORE_CERTIFICATES`: Trusted certificates in the format specified by `SSL_TRUSTSTORE_TYPE`. Default SSL engine factory supports only PEM format with X.509 certificates
* `SSL_KEY_PASSWORD`: The password of the private key in the Kafka key store file or the PEM key specified in `SSL_KEYSTORE_KEY`.

Furthermore, the following file must be present when communicating with a protected Kafka cluster:

* Key store file in the format defined in `SSL_KEYSTORE_TYPE` and at the path defined in `SSL_KEYSTORE_LOCATION`
* Key store certificate chain in PEM format at the path defined in `SSL_KEYSTORE_CERTIFICATE_CHAIN`
* Key store key in PEM format at the path defined in `SSL_KEYSTORE_KEY`
* Trust store file in the format defined in `SSL_TRUSTSTORE_TYPE` and at the path defined in `SSL_TRUSTSTORE_LOCATION`
* Trust store certificates in PEM format at the path defined in `SSL_TRUSTSTORE_CERTIFICATES`
