/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.scalatest.matchers.should.Matchers
import org.scalatest.funsuite.AnyFunSuite
import Utils.*
import KafkaTopologyUtils.*

class KafkaTopologyUtilsSpec extends AnyFunSuite with Matchers {
  test("Record with attached photo should pass preliminary tests") {
    val imageRecord = loadFile("src/test/resources/input/image_1.json")
    assert(!hasNoDigitalObject(imageRecord))
    assert(!hasNoLocator(imageRecord))
    assert(isImage(imageRecord))
  }

  test("Another record with attached photo should pass preliminary tests") {
    val imageRecord = loadFile("src/test/resources/input/image_8.json")
    assert(!hasNoDigitalObject(imageRecord))
    assert(!hasNoLocator(imageRecord))
    assert(isImage(imageRecord))
  }

  test("Record with attached video should not pass preliminary tests") {
    val imageRecord = loadFile("src/test/resources/input/video_1.json")
    assert(!hasNoDigitalObject(imageRecord))
    assert(!hasNoLocator(imageRecord))
    assert(!isImage(imageRecord))
  }

  test("Record without locator should not pass preliminary tests") {
    val imageRecord = loadFile("src/test/resources/input/no_locator.json")
    assert(!hasNoDigitalObject(imageRecord))
    assert(hasNoLocator(imageRecord))
    assert(isImage(imageRecord))
  }
}
