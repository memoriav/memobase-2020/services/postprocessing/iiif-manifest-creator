/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.scalatest.funsuite.AnyFunSuite

class ManifestV3Spec extends AnyFunSuite {
  // Ignored because of non-deterministic behavior when creating JSON lists
  ignore("a correct manifest should be built from image_1.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/image_1.json")
    )
    assert(res.get.warnings.isEmpty)
    assert(
      res.get.obj._1 == "https://memobase.ch/digital/baz-001-MEI_67484-1"
    )
    assert(
      Utils.normaliseRandomlyGeneratedIds(res.get.obj._2) == Utils.normaliseRandomlyGeneratedIds(Utils.loadFile("src/test/resources/output/v3/image_1.json"))
    )
  }

  // Ignored because of non-deterministic behavior when creating JSON lists
  ignore("a correct manifest should be built from image_2.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/image_2.json")
    )
    assert(res.get.warnings.isEmpty)
    assert(
      res.get.obj._1 == "https://memobase.ch/digital/aag-001-RBA1-4-42733_1-1"
    )
    assert(
      Utils.normaliseRandomlyGeneratedIds(res.get.obj._2) == Utils.normaliseRandomlyGeneratedIds(Utils.loadFile("src/test/resources/output/v3/image_2.json"))
    )
  }

  test("a correct manifest should be built from image_3.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/image_3.json")
    )
    assert(res.isSuccess)
    assert(res.get.warnings.isEmpty)
  }

  test("a correct manifest should be built from image_4.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/image_4.json")
    )
    assert(res.isSuccess)
    assert(res.get.warnings.isEmpty)
  }

  test("a correct manifest should be built from image_6.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/image_6.json")
    )
    assert(res.isSuccess)
    assert(res.get.warnings.isEmpty)
  }

  test("a correct manifest should be built from image_7.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/image_7.json")
    )
    assert(res.isSuccess)
    assert(res.get.warnings.isEmpty)
  }

  // Ignored because of non-deterministic behavior when creating JSON lists
  ignore("a correct manifest should be built from fernsehen.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/fernsehen.json")
    )
    assert(res.get.warnings.isEmpty)
    assert(
      Utils.normaliseRandomlyGeneratedIds(res.get.obj._2) == Utils.normaliseRandomlyGeneratedIds(Utils.loadFile("src/test/resources/output/v3/fernsehen.json"))
    )
  }

  // Ignored because of non-deterministic behavior when creating JSON lists
  ignore("a correct manifest should be built from film.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/film.json")
    )
    assert(res.get.warnings.isEmpty)
    assert(
      Utils.normaliseRandomlyGeneratedIds(res.get.obj._2) == Utils.normaliseRandomlyGeneratedIds(Utils.loadFile("src/test/resources/output/v3/film.json"))
    )
  }

  // Ignored because of non-deterministic behavior when creating JSON lists
  ignore("a correct manifest should be built from radio.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/radio.json")
    )
    assert(res.get.warnings.isEmpty)
    assert(
      Utils.normaliseRandomlyGeneratedIds(res.get.obj._2) == Utils.normaliseRandomlyGeneratedIds(Utils.loadFile("src/test/resources/output/v3/radio.json"))
    )
  }

  test("the creation of ton.json's manifest should fail because no duration is present") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/ton.json")
    )
    assert(res.isFailure)
  }

  // Ignored because of non-deterministic behavior when creating JSON lists
  ignore("a correct manifest should be built from tonbildschau.json") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/tonbildschau.json")
    )
    assert(res.get.warnings.isEmpty)
    assert(
      Utils.normaliseRandomlyGeneratedIds(res.get.obj._2) == Utils.normaliseRandomlyGeneratedIds(Utils.loadFile("src/test/resources/output/v3/tonbildschau.json"))
    )
  }

  test("a date consisting only of a year should result in a success") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/year_only_format.json")
    )
    assert(res.get.warnings.isEmpty)
  }

  test("a date consisting of a range should result in a success") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/range_date_format.json")
    )
    assert(res.get.warnings.isEmpty)
  }

  test("a date consisting only of a from date should result in a success") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/from_date_only_format.json")
    )
    assert(res.get.warnings.isEmpty)
  }

  test("a date consisting only of a until date should result in a success") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/until_date_only_format.json")
    )
    assert(res.get.warnings.isEmpty)
  }

  test("an incorrect date format should result in a warning") {
    val manifestBuilder = new ManifestV3
    val res = manifestBuilder.create(
      Utils.loadFile("src/test/resources/input/wrong_date_format.json")
    )
    assert(res.get.warnings.nonEmpty)
  }
}
