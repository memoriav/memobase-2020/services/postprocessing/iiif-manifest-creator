/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import scala.io.Source

object Utils {
  def loadFile(uri: String): String = {
    val source = Source.fromFile(uri)
    //val string = source.getLines().mkString("\n")
    //source.close
    val json = ujson.read(source.getLines().mkString)
    json.toString
  }

  def normaliseRandomlyGeneratedIds(manifest: String): String = {
    manifest
      .replaceAll("/canvas-.{4}", "/canvas-abcd")
      .replaceAll("/anno-page-.{4}", "/anno-page-abcd")
      .replaceAll("/anno-.{4}", "/anno-abcd")
  }
}
