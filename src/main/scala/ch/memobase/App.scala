/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import cats.effect.IOApp

import java.time.Duration
import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.scala.Logging
import cats.effect._
import com.comcast.ip4s._
import org.http4s.HttpRoutes
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.ember.server._

import scala.util.{Failure, Success, Try}

object App extends Logging with AppSettings with IOApp {
  private val topology = new KafkaTopology(reportingStepName)

  val streams = new KafkaStreams(
    topology.build(),
    kafkaStreamsSettings
  )
  private val healthService = HttpRoutes
    .of[IO] { case GET -> Root / "health" =>
      streams.state() match {
        case st if st.hasNotStarted => Ok("Streams application starting up")
        case st if st.isRunningOrRebalancing =>
          Ok("Streams application running")
        case st if st.isShuttingDown =>
          InternalServerError("Streams application is shutting down")
        case st if st.hasStartedOrFinishedShuttingDown =>
          InternalServerError(
            "Streams application is shutting down or has shut down"
          )
        case st if st.hasCompletedShutdown =>
          InternalServerError("Streams application has shut down")
        case _ => Ok("Unknown state")
      }
    }
    .orNotFound

  private val shutdownGracePeriodMs = 10000

  logger.trace("Setting up workflow.")
  Try(
    streams.start()
  ) match {
    case Success(_) =>
      logger.info("Workflow setup successful. Starting to process messages.")
      val streamsState = streams.state()
    case Failure(f) =>
      logger.error(
        s"Workflow setup stopped with errors: ${f.getMessage}. Aborting"
      )
      sys.exit(1)
  }

  sys.ShutdownHookThread {
    streams.close(Duration.ofMillis(shutdownGracePeriodMs))
  }

  def run(args: List[String]): IO[ExitCode] =
    EmberServerBuilder
      .default[IO]
      .withHost(ipv4"0.0.0.0")
      .withPort(port"8080")
      .withHttpApp(healthService)
      .build
      .use(_ => IO.never)
      .as(ExitCode.Success)
}
