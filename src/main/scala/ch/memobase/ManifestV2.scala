/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.rico.{DigitalObject, Record}
import de.digitalcollections.iiif.model.enums.ViewingHint
import de.digitalcollections.iiif.model.jackson.IiifObjectMapper
import de.digitalcollections.iiif.model.sharedcanvas.{
  Canvas as IIIFCanvas,
  Manifest as IIIFManifest,
  Sequence as IIIFSequence
}
import de.digitalcollections.iiif.model.{
  MimeType,
  OtherContent,
  ImageContent as IIIFImageContent
}

import java.time.{LocalTime, OffsetDateTime, ZoneOffset}
import scala.util.Try

class ManifestV2 {

  private val iiifMapper = new IiifObjectMapper()

  def create(messageValue: String): Try[ExtractionResult[(String, String)]] =
    Try {
      val record = Record(messageValue).get
      val digitalObject = record.digitalObject.get

      val imageContent = createImageContent(digitalObject)

      val canvas = createCanvas(digitalObject)
      canvas.addImage(imageContent)

      val sequence = createSequence(digitalObject.mainIdentifier)
      sequence.addCanvas(canvas)

      val manifestResult = createManifest(record, digitalObject)
      manifestResult.obj.addSequence(sequence)

      ExtractionResult(
        (
          s"https://memobase.ch/digital/${digitalObject.mainIdentifier}",
          ManifestUtils.replaceDomainNamesWithPlaceholders(
            iiifMapper.writer.writeValueAsString(manifestResult.obj)
          )
        ),
        manifestResult.warnings
      )
    }

  //noinspection DuplicatedCode
  private def createImageContent(
      digitalObject: DigitalObject
  ): IIIFImageContent = {
    val extension = digitalObject.mimeType
      .map {
        case "image/png" => "png"
        case _           => "jpg"
      }
      .get
    val imageContent = new IIIFImageContent(
      s"${ManifestUtils.mediaUriPlaceholder}/${digitalObject.mainIdentifier}/iiif/full/full/0/default.$extension"
    )
    digitalObject.usageRegulatedBy.sameAs.foreach(uri => imageContent.addLicense(uri.toString))
    digitalObject.mimeType.foreach(imageContent.setFormat)
    digitalObject.height.foreach(h => imageContent.setHeight(h.toInt))
    digitalObject.width.foreach(w => imageContent.setWidth(w.toInt))
    imageContent
  }

  //noinspection DuplicatedCode
  private def createCanvas(digitalObject: DigitalObject): IIIFCanvas = {
    val canvas = new IIIFCanvas(
      s"${ManifestUtils.manifestUriPlaceholder}/${digitalObject.mainIdentifier}/canvas/default"
    )
    canvas.addLabel("digitalObject")
    digitalObject.height.foreach(h => canvas.setHeight(h.toInt))
    digitalObject.width.foreach(w => canvas.setWidth(w.toInt))
    createThumbnail(digitalObject.mainIdentifier).foreach(
      canvas.addThumbnail(_)
    )
    canvas
  }

  private def createThumbnail(
      digitalObjectId: String
  ): Option[IIIFImageContent] = {
    val imageContent = new IIIFImageContent(
      s"${ManifestUtils.mediaUriPlaceholder}/$digitalObjectId/iiif/full/!120,120/0/default.jpg"
    )
    imageContent.setFormat(MimeType.MIME_IMAGE_JPEG)
    imageContent.setWidth(120)
    imageContent.setHeight(120)
    Some(imageContent)
  }

  private def createSequence(digitalObjectId: String): IIIFSequence = {
    val sequence = new IIIFSequence(
      s"${ManifestUtils.manifestUriPlaceholder}/$digitalObjectId/sequence/default"
    )
    sequence.addLabel("default")
    sequence.addViewingHint(ViewingHint.INDIVIDUALS)
    sequence
  }

  //noinspection ScalaStyle
  private def createManifest(
      record: Record,
      digitalObject: DigitalObject
  ): ExtractionResult[IIIFManifest] = {
    val manifest = new IIIFManifest(
      ManifestUtils.manifestId(digitalObject.mainIdentifier)("v2")
    )
    val extractionResult = ExtractionResult(manifest)
    createThumbnail(digitalObject.mainIdentifier).foreach(
      manifest.addThumbnail(_)
    )
    record.titles.foreach(title => manifest.addLabel(title.title))
    record.descriptiveNotes.foreach(note =>
      manifest.addMetadata("descriptiveNote", note)
    )
    record.scopesAndContents.foreach(v =>
      manifest.addMetadata("scopeAndContent", v)
    )
    record.publishers.foreach(publisher =>
      manifest.addMetadata("publishedBy", publisher.name)
    )
    record.placesOfCapture.foreach(place =>
      manifest.addMetadata("placeOfCapture", place.name)
    )
    record.spatialCharacteristics.foreach(place =>
      manifest.addMetadata("spatial", place.name)
    )
    record.languages.foreach(lang =>
      lang.name.foreach(caption =>
        manifest.addMetadata("hasLanguage", caption._2)
      )
    )
    record.resourceCreators.foreach(creator =>
      manifest.addMetadata(creator.roleType, s"${creator.agent.name} (${creator.roleName.mkString(", ")})")
    )
    record.producers.foreach(producer =>
      manifest.addMetadata("producer", producer.name)
    )
    record.abstracts.foreach(summary => manifest.addDescription(summary))
    record.regulatedByHolders
      .map(holder => holder.name + "\n\n")
      .zipAll(record.conditionsOfUse, "", "")
      .foreach(v => manifest.addAttribution(v._1 + v._2))
    record.creationDate
      .flatMap(_.normalizedDateValue)
      .map(ManifestUtils.createTimestamp)
      .foreach {
        case DateExtractionResult(Some(v), l, None) =>
          manifest.setNavDate(
            OffsetDateTime.of(v, LocalTime.of(0, 0), ZoneOffset.UTC)
          )
          l.foreach(v => manifest.addMetadata(v._1, v._2))
        case DateExtractionResult(None, _, Some(w)) =>
          extractionResult.warnings += w
        case _ => extractionResult.warnings += "Unknown error"
      }
    manifest.addViewingHint(ViewingHint.INDIVIDUALS)
    manifest.addRendering(
      createWebsite(record.mainIdentifier)
    )
    extractionResult
  }

  private def createWebsite(recordId: String) = {
    val websiteUri =
      s"${ManifestUtils.renderingUriPlaceholder}/object/$recordId"
    val website = new OtherContent(websiteUri)
    website.setFormat("text/html")
    website.addLabel("website")
    website
  }
}
