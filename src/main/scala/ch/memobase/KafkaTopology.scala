/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Named
import org.apache.kafka.streams.scala.ImplicitConversions.*
import org.apache.kafka.streams.scala.kstream.{Branched, KStream}
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.logging.log4j.scala.Logging
import org.apache.kafka.streams.scala.serialization.Serdes.*

import scala.util.Try

class KafkaTopology(private val step: String) extends Logging with AppSettings {

  import KafkaTopologyUtils.*

  // scalastyle:off method.length
  def build(): Topology = {
    val builder = new StreamsBuilder
    val manifestV2Builder = new ManifestV2
    val manifestV3Builder = new ManifestV3

    val source =
      builder
        .stream[String, String](kafkaInputTopic)
        .filter((k, v) => {
          logger.debug(s"Processing record $k")
          v != null
        })
    val digitalObject =
      source
        .split(Named.as("digital-object-"))
        .branch((_, v) => hasNoDigitalObject(v), Branched.as("missing"))
        .branch((_, v) => hasNoLocator(v), Branched.as("no-locator"))
        .branch(
          (_, v) => isNotDereferencable(v),
          Branched.as("not-dereferencable")
        )
        .defaultBranch(Branched.as("dereferencable"))

    digitalObject
      .get("digital-object-no-locator")
      .foreach(
        reportIgnoredRecord(
          _,
          kafkaReportTopic,
          "Digital object has no locator"
        )
      )
    digitalObject
      .get("digital-object-not-dereferencable")
      .foreach(
        reportIgnoredRecord(
          _,
          kafkaReportTopic,
          "Resource is not locally available"
        )
      )
    digitalObject
      .get("digital-object-missing")
      .foreach(
        reportIgnoredRecord(_, kafkaReportTopic, "Record has no digital object")
      )

    // noinspection ConvertibleToMethodValue
    val mayBeImage = digitalObject
      .get("digital-object-dereferencable")
      .map(
        _.split(Named.as("image-"))
          .branch((_, v) => isImage(v), Branched.as("true"))
          .defaultBranch(Branched.as("false"))
      )
      .getOrElse(Map())

    mayBeImage
      .get("image-false")
      .foreach(
        reportIgnoredRecord(
          _,
          kafkaReportTopic,
          "Record describes no image, thus no Manifest for v2 is built"
        )
      )

    val manifestV2 = mayBeImage
      .get("image-true")
      .map(
        _.filter((_, v) => isImage(v))
          .mapValues(manifestV2Builder.create)
          .split(Named.as("iiif-v2-"))
          .branch(
            (_, v) => v.isSuccess && v.get.warnings.nonEmpty,
            Branched.as("warning")
          )
          .branch((_, v) => v.isSuccess, Branched.as("success"))
          .defaultBranch(Branched.as("failure"))
      )
      .getOrElse(Map())

    manifestV2.get("iiif-v2-warning").foreach(sendRecord(_, kafkaV2OutputTopic))
    manifestV2.get("iiif-v2-success").foreach(sendRecord(_, kafkaV2OutputTopic))
    manifestV2
      .get("iiif-v2-warning")
      .foreach(reportManifestCreationWarnings(_, kafkaReportTopic, "v2"))
    manifestV2
      .get("iiif-v2-success")
      .foreach(reportSuccessfulManifestCreation(_, kafkaReportTopic, "v2"))
    manifestV2
      .get("iiif-v2-failure")
      .foreach(reportManifestCreationFailure(_, kafkaReportTopic, "v2"))

    // noinspection ConvertibleToMethodValue
    val manifestV3 =
      digitalObject
        .get("digital-object-dereferencable")
        .map(
          _.mapValues(manifestV3Builder.create(_))
            .split(Named.as("iiif-v3-"))
            .branch(
              (_, v) => v.isSuccess && v.get.warnings.nonEmpty,
              Branched.as("warning")
            )
            .branch((_, v) => v.isSuccess, Branched.as("success"))
            .defaultBranch(Branched.as("failure"))
        )
        .getOrElse(Map())
    manifestV3.get("iiif-v3-warning").foreach(sendRecord(_, kafkaV3OutputTopic))
    manifestV3.get("iiif-v3-success").foreach(sendRecord(_, kafkaV3OutputTopic))
    manifestV3
      .get("iiif-v3-warning")
      .foreach(reportManifestCreationWarnings(_, kafkaReportTopic, "v3"))
    manifestV3
      .get("iiif-v3-success")
      .foreach(reportSuccessfulManifestCreation(_, kafkaReportTopic, "v3"))
    manifestV3
      .get("iiif-v3-failure")
      .foreach(reportManifestCreationFailure(_, kafkaReportTopic, "v3"))

    builder.build()
  }
  // scalastyle:on

  private def sendRecord(
      stream: KStream[String, Try[ExtractionResult[(String, String)]]],
      topicOut: String
  ): Unit = {
    stream
      .map((_, v) => (extractId(v.get.obj._1), v.get.obj._2))
      .to(topicOut)
  }

  private def reportManifestCreationWarnings(
      stream: KStream[String, Try[ExtractionResult[(String, String)]]],
      topicReport: String,
      manifestVersion: String
  ): Unit =
    stream
      .map((k, v) => {
        logger.warn(
          s"Manifest $manifestVersion creation for record $k ended with warnings: ${v.get.warnings.mkString("\n")}"
        )
        (
          k,
          ReportingObject(
            s"$k-$manifestVersion",
            ProcessingWarning,
            s"Manifest $manifestVersion creation ended with warnings:\n${v.get.warnings
                .mkString("\n")}",
            step,
            getStepVersion
          ).toString
        )
      })
      .to(topicReport)

  private def reportSuccessfulManifestCreation(
      stream: KStream[String, Try[ExtractionResult[(String, String)]]],
      topicReport: String,
      manifestVersion: String
  ): Unit =
    stream
      .map((k, _) => {
        logger.debug(
          s"Successfully created manifest $manifestVersion for record $k"
        )
        (
          k,
          ReportingObject(
            s"$k-$manifestVersion",
            ProcessingSuccess,
            s"IIIF manifest $manifestVersion successfully created",
            step,
            getStepVersion
          ).toString
        )
      })
      .to(topicReport)

  private def reportManifestCreationFailure(
      stream: KStream[String, Try[ExtractionResult[(String, String)]]],
      topicReport: String,
      manifestVersion: String
  ): Unit =
    stream
      .map((k, v) => {
        logger.error(
          s"Error creating manifest $manifestVersion for record $k: ${v.failed.get.getMessage}"
        )
        (
          s"$k-$manifestVersion",
          ReportingObject(
            s"$k-$manifestVersion",
            ProcessingFatal,
            s"Error creating manifest $manifestVersion: ${v.failed.get.getMessage}",
            step,
            getStepVersion
          ).toString
        )
      })
      .to(topicReport)

  private def reportIgnoredRecord(
      stream: KStream[String, String],
      topicReport: String,
      message: String
  ): Unit = {
    List("v2", "v3").foreach { v =>
      stream
        .map((k, _) => {
          logger.debug(s"No manifest $v built for record $k: $message")
          (
            s"$k-$v",
            ReportingObject(
              s"$k-$v",
              ProcessingIgnore,
              message,
              step,
              getStepVersion
            ).toString
          )
        })
        .to(topicReport)
    }
  }

  private val getStepVersion: String =
    getClass.getPackage.getImplementationVersion
}
