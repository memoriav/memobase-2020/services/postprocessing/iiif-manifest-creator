/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.rico.{
  DigitalObject,
  FilmType,
  FotoType,
  PhysicalObject,
  RadioType,
  Record,
  TVType,
  TonType,
  TonbildschauType,
  VideoType
}
import info.freelibrary.iiif.presentation.v3.ids.{Minter, MinterFactory}
import info.freelibrary.iiif.presentation.v3.properties.*
import info.freelibrary.iiif.presentation.v3.properties.behaviors.{
  CanvasBehavior,
  ManifestBehavior
}
import info.freelibrary.iiif.presentation.v3.{
  Canvas,
  ImageContent,
  Manifest,
  MediaType,
  SoundContent,
  VideoContent
}
import org.apache.logging.log4j.scala.Logging

import java.net.URI
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}

class ManifestV3 extends Logging {

  // scalastyle:off method.length
  def create(messageValue: String): Try[ExtractionResult[(String, String)]] =
    Try {
      val record = Record(messageValue).get
      val physicalObject = record.physicalObject.get
      val digitalObject = record.digitalObject.get
      // FIXME: For now, its value is always "none", since there is no language field yet on documents
      val lang = "none"
      val i18nLabel = createI18nLabel(lang)
      val i18nValue = createI18nValue(lang)

      val manifest = createManifest(record, digitalObject, i18nLabel)
      val minter = MinterFactory.getMinter(manifest.obj)
      val ExtractionResult(mainCanvas, canvasWarnings) = createCanvas(
        minter,
        digitalObject.height,
        digitalObject.width,
        digitalObject.duration orElse physicalObject.duration
      )
      manifest.warnings ++= canvasWarnings

      val manifestMetadata = createManifestMetadata(record, i18nValue)
      manifest.obj.setMetadata(manifestMetadata.obj.toList*)
      manifest.warnings ++= manifestMetadata.warnings
      manifest.obj.setCanvases(mainCanvas)

      record.ricoType match {
        case FilmType | TVType | VideoType | TonbildschauType =>
          val ExtractionResult(video, videoWarnings) =
            createVideoContent(record, digitalObject, physicalObject)
          manifest.warnings ++= videoWarnings
          mainCanvas.paintWith(minter, video)
          ManifestV3Utils
            .createAdditionalImage(
              digitalObject,
              "video",
              thumbnailSize = false
            )
            .foreach { posterImage =>
              {
                val placeholderCanvas = createCanvas(
                  minter,
                  posterImage.getWidth,
                  posterImage.getWidth
                )
                placeholderCanvas.paintWith(minter, posterImage)
                manifest.obj.addCanvases(placeholderCanvas)
              }
            }
          ManifestV3Utils
            .createAdditionalImage(digitalObject, "video", thumbnailSize = true)
            .foreach(t => mainCanvas.setThumbnails(t))
        case TonType | RadioType =>
          val ExtractionResult(sound, soundWarnings) =
            createSoundContent(record, digitalObject, physicalObject)
          mainCanvas.paintWith(minter, sound)
          manifest.warnings ++= soundWarnings
        case FotoType =>
          val image = createImageContent(record, digitalObject)
          mainCanvas.paintWith(minter, image)
          ManifestV3Utils
            .createAdditionalImage(digitalObject, "image", thumbnailSize = true)
            .foreach(t => mainCanvas.setThumbnails(t))
      }

      ExtractionResult(
        (
          s"https://memobase.ch/digital/${digitalObject.mainIdentifier}",
          ManifestUtils.replaceDomainNamesWithPlaceholders(
            ujson.read(manifest.obj.toString).toString
          )
        ),
        manifest.warnings
      )
    }
  // scalastyle:on

  private def createManifest(
      record: Record,
      digitalObject: DigitalObject,
      labelFun: String => Label
  ): ExtractionResult[Manifest] = {
    val manifestId =
      ManifestUtils.manifestId(digitalObject.mainIdentifier)("v3")
    val title = record.titles.head
    val manifest = new Manifest(manifestId, labelFun(title.title))

    manifest.addBehaviors(
      ManifestBehavior.NO_AUTO_ADVANCE,
      ManifestBehavior.NO_REPEAT,
      ManifestBehavior.INDIVIDUALS
    )

    record.abstracts.foreach(manifest.setSummary)
    val extractionResult = ExtractionResult(manifest)

    Try {
      manifest.setProviders(createProvider(record))
    } recover { err =>
      extractionResult.warnings.append(
        s"Provider creation error on manifest: ${err.getMessage}"
      )
    }

    Try {
      manifest.setHomepages(homepage)
    } recover { err =>
      extractionResult.warnings.append(
        s"Homepage creation error on manifest: ${err.getMessage}"
      )
    }
    extractionResult
  }

  private def createCanvas(
      minter: Minter,
      height: Option[Double],
      width: Option[Double],
      duration: Option[String]
  ): ExtractionResult[Canvas] = {
    val canvas = new Canvas(minter)
    canvas.addBehaviors(
      CanvasBehavior.NO_AUTO_ADVANCE,
      CanvasBehavior.NON_PAGED
    )
    for { h <- height; w <- width } yield canvas.setWidthHeight(
      w.toInt,
      h.toInt
    )
    duration.map(d =>
      ManifestUtils.parseDuration(d) match {
        case DurationExtractionSuccess(v) =>
          ExtractionResult(canvas.setDuration(v))
        case DurationExtractionWarning(err) =>
          ExtractionResult(canvas, ListBuffer(err))
      }
    ) getOrElse { ExtractionResult(canvas) }

  }

  private def createCanvas(minter: Minter, height: Int, width: Int): Canvas = {
    val canvas = new Canvas(minter)
    canvas.addBehaviors(
      CanvasBehavior.NO_AUTO_ADVANCE,
      CanvasBehavior.NON_PAGED
    )
    canvas.setWidthHeight(width, height)
  }

  private def createProvider(record: Record): Provider = {
    val id = record.holders.head
    val uri =
      new URI(s"${ManifestUtils.renderingUriPlaceholder}/institution/$id")
    val provider = new Provider(uri, createI18nLabel("none")(id))
    val germanCaption = new I18n("de", "Institution @ Memobase")
    val frenchCaption = new I18n("fr", "Institution @ Memobase")
    val italianCaption = new I18n("it", "Institution @ Memobase")
    val label = new Label(germanCaption, frenchCaption, italianCaption)
    val homepage = new Homepage(uri, label)
      .setLanguages("de", "fr", "it")
      .setFormat(MediaType.TEXT_HTML)
    provider.setHomepages(homepage)
  }

  private val homepage: Homepage = {
    val germanCaption = new I18n("de", "MEMOBASE von Memoriav")
    val frenchCaption = new I18n("fr", "MEMOBASE de Memoriav")
    val italianCaption = new I18n("it", "MEMOBASE di Memoriav")
    val label = new Label(germanCaption, frenchCaption, italianCaption)
    new Homepage("https://memobase.ch", label)
  }

  // scalastyle:off cyclomatic.complexity
  private def createManifestMetadata(
      record: Record,
      createI18nValue: String => Value
  ): ExtractionResult[mutable.ListBuffer[Metadata]] = {
    val metadata: ExtractionResult[mutable.ListBuffer[Metadata]] =
      ExtractionResult(mutable.ListBuffer())
    record.descriptiveNotes
      .map(createI18nValue)
      .map(createDescriptiveNoteMetadata)
      .foreach {
        case Success(v) => metadata.obj.append(v)
        case Failure(err) =>
          metadata.warnings.append(
            s"Error when creating descriptive note metadata: ${err.getMessage}"
          )
      }
    record.scopesAndContents
      .map(createI18nValue)
      .map(createScopeAndContentMetadata)
      .foreach {
        case Success(v) => metadata.obj.append(v)
        case Failure(err) =>
          metadata.warnings.append(
            s"Error when creating scope and content metadata: ${err.getMessage}"
          )
      }
    record.publishers
      .map(_.name)
      .map(createI18nValue)
      .map(createHasPublisherMetadata)
      .foreach {
        case Success(v) => metadata.obj.append(v)
        case Failure(err) =>
          metadata.warnings.append(
            s"Error when creating publisher metadata: ${err.getMessage}"
          )
      }
    record.creationDate
      .flatMap(_.normalizedDateValue)
      .map(createCreationDateMetadata)
      .foreach {
        case Success(v) => v.foreach(e => metadata.obj.append(e))
        case Failure(err) =>
          metadata.warnings.append(
            s"Error when creating creation date metadata: ${err.getMessage}"
          )
      }
    record.placesOfCapture
      .map(_.name)
      .map(createI18nValue)
      .map(createPlaceOfCaptureMetadata)
      .foreach {
        case Success(v) => metadata.obj.append(v)
        case Failure(err) =>
          metadata.warnings.append(
            s"Error when creating place of capture metadata: ${err.getMessage}"
          )
      }
    record.spatialCharacteristics
      .map(_.name)
      .map(createI18nValue)
      .map(createSpatialMetadata)
      .foreach {
        case Success(v) => metadata.obj.append(v)
        case Failure(err) =>
          metadata.warnings.append(
            s"Error when creating spatial metadata: ${err.getMessage}"
          )
      }
    record.languages
      .flatMap(lang => lang.name.map(_._2))
      .map(createI18nValue)
      .map(createHasOrHadLanguageMetadata)
      .foreach {
        case Success(v) => metadata.obj.append(v)
        case Failure(err) =>
          metadata.warnings.append(
            s"Error when creating language metadata: ${err.getMessage}"
          )
      }
    record.resourceCreators
      .map(creator =>
        (
          creator.roleType,
          createI18nValue(s"${creator.agent.name} (${creator.roleName.mkString(", ")})")
        )
      )
      .map(v => createResourceCreatorMetadata(v._1, v._2))
      .foreach {
        case Success(v) => metadata.obj.append(v)
        case Failure(err) =>
          metadata.warnings.append(
            s"Error when creating resource creator metadata: ${err.getMessage}"
          )
      }
    metadata
  }
  // scalastyle:on

  private def createDescriptiveNoteMetadata(value: Value) =
    Try {
      val germanCaption = new I18n("de", "Bemerkung")
      val frenchCaption = new I18n("fr", "Remarque")
      val italianCaption = new I18n("it", "Osservazione")
      val label = new Label(germanCaption, frenchCaption, italianCaption)
      new Metadata(label, value)
    }

  private def createScopeAndContentMetadata(value: Value) =
    Try {
      val germanCaption = new I18n("de", "Entstehungsumstände")
      val frenchCaption = new I18n("fr", "Circonstances d'origine")
      val italianCaption = new I18n("it", "Circostanze di origine")
      val label = new Label(germanCaption, frenchCaption, italianCaption)
      new Metadata(label, value)
    }

  private def createHasPublisherMetadata(value: Value) =
    Try {
      val germanCaption = new I18n("de", "Verlag")
      val frenchCaption = new I18n("fr", "Éditeur")
      val italianCaption = new I18n("it", "Editore")
      val label = new Label(germanCaption, frenchCaption, italianCaption)
      new Metadata(label, value)
    }

  private def createCreationDateMetadata(value: String): Try[List[Metadata]] =
    ManifestUtils.createTimestamp(value) match {
      case DateExtractionResult(Some(v), l, None) =>
        Try {
          l.map(v => {
            val rangeLabel = new Label("en", v._1)
            new Metadata(rangeLabel, createI18nValue("none")(v._2))
          }) :+ {
            val germanCaption = new I18n("de", "Datum Veröffentlichung")
            val frenchCaption = new I18n("fr", "Date de publication")
            val italianCaption = new I18n("it", "Data di pubblicazione")
            val publicationDateLabel =
              new Label(germanCaption, frenchCaption, italianCaption)
            new Metadata(
              publicationDateLabel,
              createI18nValue("none")(v.toString)
            )
          }
        }
      case DateExtractionResult(None, _, Some(w)) =>
        Failure(new Exception(w))
      case _ =>
        Failure(new Exception("Unknown date error"))
    }

  private def createPlaceOfCaptureMetadata(value: Value) =
    Try {
      val germanCaption = new I18n("de", "Aufnahmeort")
      val frenchCaption = new I18n("fr", "Lieu d'enregistrement")
      val italianCaption = new I18n("it", "Luogo di registrazione")
      val label = new Label(germanCaption, frenchCaption, italianCaption)
      new Metadata(label, value)
    }

  private def createSpatialMetadata(value: Value) =
    Try {
      val germanCaption = new I18n("de", "Räumliche Abdeckung")
      val frenchCaption = new I18n("fr", "Couverture spatiale")
      val italianCaption = new I18n("it", "Copertura spaziale")
      val label = new Label(germanCaption, frenchCaption, italianCaption)
      new Metadata(label, value)
    }

  private def createHasOrHadLanguageMetadata(value: Value) =
    Try {
      val germanCaption = new I18n("de", "Sprache")
      val frenchCaption = new I18n("fr", "Langue")
      val italianCaption = new I18n("it", "Lingua")
      val label = new Label(germanCaption, frenchCaption, italianCaption)
      new Metadata(label, value)
    }

  private def createResourceCreatorMetadata(
      label: String,
      value: Value
  ): Try[Metadata] =
    Try {
      val caption = new Label(new I18n("none", label))
      new Metadata(caption, value)
    }

  private def createVideoContent(
      record: Record,
      digitalObject: DigitalObject,
      physicalObject: PhysicalObject
  ): ExtractionResult[VideoContent] = {

    val id =
      s"${ManifestUtils.mediaUriPlaceholder}/${digitalObject.mainIdentifier}/master"

    val videoContent = new VideoContent(id)
    record.abstracts.foreach(videoContent.setSummary)
    digitalObject.usageRegulatedBy.sameAs.foreach(videoContent.setRights)
    digitalObject.mimeType
      .flatMap(ManifestV3Utils.getFormat)
      .foreach(videoContent.setFormat)
    createRequiredStatement(record).foreach(v =>
      videoContent.setRequiredStatement(v)
    )
    record.conditionsOfUse.foreach(condition =>
      videoContent.setRequiredStatement(
        new RequiredStatement("Conditions of use", condition)
      )
    )
    for {
      width <- digitalObject.width
      height <- digitalObject.height
    } yield videoContent.setWidthHeight(width.toInt, height.toInt)

    (digitalObject.duration orElse physicalObject.duration)
      .map(d =>
        ManifestUtils.parseDuration(d) match {
          case DurationExtractionSuccess(v) =>
            ExtractionResult(videoContent.setDuration(v))
          case DurationExtractionWarning(err) =>
            ExtractionResult(videoContent, ListBuffer(err))
        }
      ) getOrElse {
      ExtractionResult(videoContent)
    }
  }

  private def createSoundContent(
      record: Record,
      digitalObject: DigitalObject,
      physicalObject: PhysicalObject
  ): ExtractionResult[SoundContent] = {
    val id =
      s"${ManifestUtils.mediaUriPlaceholder}/${digitalObject.mainIdentifier}/master"

    val soundContent = new SoundContent(id)
    record.abstracts.foreach(soundContent.setSummary)
    digitalObject.usageRegulatedBy.sameAs.foreach(soundContent.setRights)
    digitalObject.mimeType
      .flatMap(ManifestV3Utils.getFormat)
      .foreach(soundContent.setFormat)
    createRequiredStatement(record).foreach(v =>
      soundContent.setRequiredStatement(v)
    )
    (digitalObject.duration orElse physicalObject.duration)
      .map(d =>
        ManifestUtils.parseDuration(d) match {
          case DurationExtractionSuccess(v) =>
            ExtractionResult(soundContent.setDuration(v))
          case DurationExtractionWarning(err) =>
            ExtractionResult(soundContent, ListBuffer(err))
        }
      ) getOrElse {
      ExtractionResult(soundContent)
    }
  }

  private def createImageContent(
      record: Record,
      digitalObject: DigitalObject
  ): ImageContent = {
    val id =
      s"${ManifestUtils.mediaUriPlaceholder}/${digitalObject.mainIdentifier}/iiif/full/full/0/default.jpg"
    val imageContent = new ImageContent(id)
    imageContent.setMetadata(
      ManifestV3Utils.createContentResourceMetadata(digitalObject)*
    )
    record.abstracts.foreach(imageContent.setSummary)
    digitalObject.usageRegulatedBy.sameAs.foreach(v =>
      imageContent.setRights(v)
    )
    digitalObject.mimeType
      .flatMap(f => ManifestV3Utils.getFormat(f))
      .foreach(v => imageContent.setFormat(v))
    createRequiredStatement(record).foreach(v =>
      imageContent.setRequiredStatement(v)
    )

    for {
      width <- digitalObject.width
      height <- digitalObject.height
    } yield imageContent.setWidthHeight(width.toInt, height.toInt)
    imageContent
  }

  private def createRequiredStatement(
      record: Record
  ): Option[RequiredStatement] = {
    (
      record.conditionsOfUse.map(v => s"Conditions of use: $v"),
      record.regulatedByHolders.map(v => s"Attribution: $v")
    ) match {
      case (conditionsOfUse, attribution)
          if conditionsOfUse.nonEmpty && attribution.nonEmpty =>
        Some(
          new RequiredStatement(
            "Rights statement",
            f"${conditionsOfUse.head}; ${attribution.head}"
          )
        )
      case (conditionsOfUse, _) if conditionsOfUse.nonEmpty =>
        Some(new RequiredStatement("Rights statement", conditionsOfUse.head))
      case (_, attribution) if attribution.nonEmpty =>
        Some(new RequiredStatement("Rights statement", attribution.head))
      case _ => None
    }
  }

  private def createI18nLabel(lang: String)(value: String): Label =
    new Label(lang, value)

  private def createI18nValue(lang: String)(value: String): Value =
    new Value(lang, value)

}
