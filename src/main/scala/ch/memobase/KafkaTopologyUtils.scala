/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.rico.{FotoType, Record}

import scala.util.Try
import scala.util.Success

object KafkaTopologyUtils {

  def extractId(uri: String): String =
    uri.split("/").last.split("\\.(?=[^.]+$)")(0)

  def isImage(msgVal: String): Boolean =
    Record(msgVal).map(_.ricoType == FotoType).getOrElse(false)

  def isNotDereferencable(msgVal: String): Boolean =
    Record(msgVal) match {
      case Success(rec) =>
        rec.digitalObject.forall(!_.locator.startsWith("sftp:"))
      case _ => true
    }

  def hasNoDigitalObject(msgVal: String): Boolean =
    Record(msgVal) match {
      case Success(rec) => rec.digitalObject.isEmpty
      case _            => true
    }

  def hasNoLocator(msgVal: String): Boolean =
    Record(msgVal) match {
      case Success(rec) => rec.digitalObject.flatMap(obj => Try(obj.locator).toOption).isEmpty
      case _            => true
    }

}
