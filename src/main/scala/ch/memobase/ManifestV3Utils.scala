/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.rico.DigitalObject
import info.freelibrary.iiif.presentation.v3.properties.Metadata
import info.freelibrary.iiif.presentation.v3.{ImageContent, MediaType}

object ManifestV3Utils {

  def createAdditionalImage(
      digitalObject: DigitalObject,
      mediaType: String,
      thumbnailSize: Boolean
  ): Option[ImageContent] = {
    val dimensions = if (thumbnailSize) "!120,120" else "max"
    val createId = mediaType match {
      case "image" =>
        (id: String) =>
          s"${ManifestUtils.mediaUriPlaceholder}/$id/iiif/full/$dimensions/0/default.jpg"
      case "video" =>
        (id: String) =>
          s"${ManifestUtils.mediaUriPlaceholder}/$id-poster/iiif/full/$dimensions/0/default.jpg"
    }
    val uri = createId(digitalObject.mainIdentifier)
    val imageContent = new ImageContent(uri)
    imageContent.setFormat(MediaType.IMAGE_JPEG)
    if (thumbnailSize) {
      imageContent.setWidthHeight(120, 120)
    } else {
      for {
        h <- digitalObject.height
        w <- digitalObject.width
      } yield imageContent.setWidthHeight(w.toInt, h.toInt)
    }
    Some(imageContent)
  }

  def createContentResourceMetadata(
      digitalObject: DigitalObject
  ): List[Metadata] =
    List(
      new Metadata(
        "Component color",
        digitalObject.componentColors.mkString(", ")
      )
    )

  // scalastyle:off cyclomatic.complexity method.length
  def getFormat(mimeString: String): Option[MediaType] =
    mimeString match {
      case "application/atom+xml" =>
        Some(MediaType.APPLICATION_ATOM_PLUS_XML)
      case "application/atomcat+xml" =>
        Some(MediaType.APPLICATION_ATOMCAT_PLUS_XML)
      case "application/atomsvc+xml" =>
        Some(MediaType.APPLICATION_ATOMSVC_PLUS_XML)
      case "application/cellml+xml" =>
        Some(MediaType.APPLICATION_CELLML_PLUS_XML)
      case "application/cu-seeme" => Some(MediaType.APPLICATION_CU_SEEME)
      case "application/dash+xml" =>
        Some(MediaType.APPLICATION_DASH_PLUS_XML)
      case "application/dssc+der" =>
        Some(MediaType.APPLICATION_DSSC_PLUS_DER)
      case "application/dssc+xml" =>
        Some(MediaType.APPLICATION_DSSC_PLUS_XML)
      case "application/ecmascript" =>
        Some(MediaType.APPLICATION_ECMASCRIPT)
      case "application/emma+xml" =>
        Some(MediaType.APPLICATION_EMMA_PLUS_XML)
      case "application/epub+zip" =>
        Some(MediaType.APPLICATION_EPUB_PLUS_ZIP)
      case "application/exi"       => Some(MediaType.APPLICATION_EXI)
      case "application/font-sfnt" => Some(MediaType.APPLICATION_FONT_SFNT)
      case "application/gltf-buffer" =>
        Some(MediaType.APPLICATION_GLTF_BUFFER)
      case "application/gpx+xml" => Some(MediaType.APPLICATION_GPX_PLUS_XML)
      case "application/inkml+xml" =>
        Some(MediaType.APPLICATION_INKML_PLUS_XML)
      case "application/java-archive" =>
        Some(MediaType.APPLICATION_JAVA_ARCHIVE)
      case "application/java-serialized-object" =>
        Some(MediaType.APPLICATION_JAVA_SERIALIZED_OBJECT)
      case "application/java-vm" => Some(MediaType.APPLICATION_JAVA_VM)
      case "application/javascript" =>
        Some(MediaType.APPLICATION_JAVASCRIPT)
      case "application/json"    => Some(MediaType.APPLICATION_JSON)
      case "application/ld+json" => Some(MediaType.APPLICATION_LD_PLUS_JSON)
      case "application/mads+xml" =>
        Some(MediaType.APPLICATION_MADS_PLUS_XML)
      case "application/marc" => Some(MediaType.APPLICATION_MARC)
      case "application/marcxml+xml" =>
        Some(MediaType.APPLICATION_MARCXML_PLUS_XML)
      case "application/mathematica" =>
        Some(MediaType.APPLICATION_MATHEMATICA)
      case "application/mathml+xml" =>
        Some(MediaType.APPLICATION_MATHML_PLUS_XML)
      case "application/mbox" => Some(MediaType.APPLICATION_MBOX)
      case "application/metalink4+xml" =>
        Some(MediaType.APPLICATION_METALINK4_PLUS_XML)
      case "application/mets+xml" =>
        Some(MediaType.APPLICATION_METS_PLUS_XML)
      case "application/mods+xml" =>
        Some(MediaType.APPLICATION_MODS_PLUS_XML)
      case "application/mp21"   => Some(MediaType.APPLICATION_MP21)
      case "application/msword" => Some(MediaType.APPLICATION_MSWORD)
      case "application/mxf"    => Some(MediaType.APPLICATION_MXF)
      case "application/octet-stream" =>
        Some(MediaType.APPLICATION_OCTET_STREAM)
      case "application/oebps-package+xml" =>
        Some(MediaType.APPLICATION_OEBPS_PACKAGE_PLUS_XML)
      case "application/ogg"     => Some(MediaType.APPLICATION_OGG)
      case "application/onenote" => Some(MediaType.APPLICATION_ONENOTE)
      case "application/p21"     => Some(MediaType.APPLICATION_P21)
      case "application/patch-ops-error+xml" =>
        Some(MediaType.APPLICATION_PATCH_OPS_ERROR_PLUS_XML)
      case "application/pdf"      => Some(MediaType.APPLICATION_PDF)
      case "application/pgp-keys" => Some(MediaType.APPLICATION_PGP_KEYS)
      case "application/pkcs12"   => Some(MediaType.APPLICATION_PKCS12)
      case "application/pkcs7-mime" =>
        Some(MediaType.APPLICATION_PKCS7_MIME)
      case "application/postscript" =>
        Some(MediaType.APPLICATION_POSTSCRIPT)
      case "application/prs.cww" => Some(MediaType.APPLICATION_PRS_CWW)
      case "application/prs.nprend" =>
        Some(MediaType.APPLICATION_PRS_NPREND)
      case "application/rdf+xml" => Some(MediaType.APPLICATION_RDF_PLUS_XML)
      case "application/relax-ng-compact-syntax" =>
        Some(MediaType.APPLICATION_RELAX_NG_COMPACT_SYNTAX)
      case "application/resource-lists-diff+xml" =>
        Some(MediaType.APPLICATION_RESOURCE_LISTS_DIFF_PLUS_XML)
      case "application/resource-lists+xml" =>
        Some(MediaType.APPLICATION_RESOURCE_LISTS_PLUS_XML)
      case "application/rls-services+xml" =>
        Some(MediaType.APPLICATION_RLS_SERVICES_PLUS_XML)
      case "application/rss+xml" => Some(MediaType.APPLICATION_RSS_PLUS_XML)
      case "application/rtf"     => Some(MediaType.APPLICATION_RTF)
      case "application/sarif-external-properties+json" =>
        Some(MediaType.APPLICATION_SARIF_EXTERNAL_PROPERTIES_PLUS_JSON)
      case "application/sarif+json" =>
        Some(MediaType.APPLICATION_SARIF_PLUS_JSON)
      case "application/sbml+xml" =>
        Some(MediaType.APPLICATION_SBML_PLUS_XML)
      case "application/schema+json" =>
        Some(MediaType.APPLICATION_SCHEMA_PLUS_JSON)
      case "application/sieve" => Some(MediaType.APPLICATION_SIEVE)
      case "application/smil+xml" =>
        Some(MediaType.APPLICATION_SMIL_PLUS_XML)
      case "application/sparql-query" =>
        Some(MediaType.APPLICATION_SPARQL_QUERY)
      case "application/sparql-results+xml" =>
        Some(MediaType.APPLICATION_SPARQL_RESULTS_PLUS_XML)
      case "application/srgs" => Some(MediaType.APPLICATION_SRGS)
      case "application/srgs+xml" =>
        Some(MediaType.APPLICATION_SRGS_PLUS_XML)
      case "application/sru+xml" => Some(MediaType.APPLICATION_SRU_PLUS_XML)
      case "application/ssml+xml" =>
        Some(MediaType.APPLICATION_SSML_PLUS_XML)
      case "application/tei+xml" => Some(MediaType.APPLICATION_TEI_PLUS_XML)
      case "application/vnd.3m.post-it-notes" =>
        Some(MediaType.APPLICATION_VND_3M_POST_IT_NOTES)
      case "application/vnd.acucorp" =>
        Some(MediaType.APPLICATION_VND_ACUCORP)
      case "application/vnd.adobe.fxp" =>
        Some(MediaType.APPLICATION_VND_ADOBE_FXP)
      case "application/vnd.adobe.xdp+xml" =>
        Some(MediaType.APPLICATION_VND_ADOBE_XDP_PLUS_XML)
      case "application/vnd.adobe.xfdf" =>
        Some(MediaType.APPLICATION_VND_ADOBE_XFDF)
      case "application/vnd.afpc.modca" =>
        Some(MediaType.APPLICATION_VND_AFPC_MODCA)
      case "application/vnd.amazon.ebook" =>
        Some(MediaType.APPLICATION_VND_AMAZON_EBOOK)
      case "application/vnd.android.package-archive" =>
        Some(MediaType.APPLICATION_VND_ANDROID_PACKAGE_ARCHIVE)
      case "application/vnd.apple.installer+xml" =>
        Some(MediaType.APPLICATION_VND_APPLE_INSTALLER_PLUS_XML)
      case "application/vnd.apple.mpegurl" =>
        Some(MediaType.APPLICATION_VND_APPLE_MPEGURL)
      case "application/vnd.audiograph" =>
        Some(MediaType.APPLICATION_VND_AUDIOGRAPH)
      case "application/vnd.bmi" => Some(MediaType.APPLICATION_VND_BMI)
      case "application/vnd.chemdraw+xml" =>
        Some(MediaType.APPLICATION_VND_CHEMDRAW_PLUS_XML)
      case "application/vnd.cinderella" =>
        Some(MediaType.APPLICATION_VND_CINDERELLA)
      case "application/vnd.clonk.c4group" =>
        Some(MediaType.APPLICATION_VND_CLONK_C4GROUP)
      case "application/vnd.commerce-battelle" =>
        Some(MediaType.APPLICATION_VND_COMMERCE_BATTELLE)
      case "application/vnd.commonspace" =>
        Some(MediaType.APPLICATION_VND_COMMONSPACE)
      case "application/vnd.coreos.ignition+json" =>
        Some(MediaType.APPLICATION_VND_COREOS_IGNITION_PLUS_JSON)
      case "application/vnd.cryptomator.encrypted" =>
        Some(MediaType.APPLICATION_VND_CRYPTOMATOR_ENCRYPTED)
      case "application/vnd.cups-ppd" =>
        Some(MediaType.APPLICATION_VND_CUPS_PPD)
      case "application/vnd.data-vision.rdz" =>
        Some(MediaType.APPLICATION_VND_DATA_VISION_RDZ)
      case "application/vnd.debian.binary-package" =>
        Some(MediaType.APPLICATION_VND_DEBIAN_BINARY_PACKAGE)
      case "application/vnd.dece.data" =>
        Some(MediaType.APPLICATION_VND_DECE_DATA)
      case "application/vnd.dece.ttml+xml" =>
        Some(MediaType.APPLICATION_VND_DECE_TTML_PLUS_XML)
      case "application/vnd.dece.unspecified" =>
        Some(MediaType.APPLICATION_VND_DECE_UNSPECIFIED)
      case "application/vnd.dece.zip" =>
        Some(MediaType.APPLICATION_VND_DECE_ZIP)
      case "application/vnd.dolby.mlp" =>
        Some(MediaType.APPLICATION_VND_DOLBY_MLP)
      case "application/vnd.dpgraph" =>
        Some(MediaType.APPLICATION_VND_DPGRAPH)
      case "application/vnd.dreamfactory" =>
        Some(MediaType.APPLICATION_VND_DREAMFACTORY)
      case "application/vnd.dvb.ait" =>
        Some(MediaType.APPLICATION_VND_DVB_AIT)
      case "application/vnd.dvb.service" =>
        Some(MediaType.APPLICATION_VND_DVB_SERVICE)
      case "application/vnd.dynageo" =>
        Some(MediaType.APPLICATION_VND_DYNAGEO)
      case "application/vnd.ericsson.quickcall" =>
        Some(MediaType.APPLICATION_VND_ERICSSON_QUICKCALL)
      case "application/vnd.eszigno3+xml" =>
        Some(MediaType.APPLICATION_VND_ESZIGNO3_PLUS_XML)
      case "application/vnd.etsi.asic-e+zip" =>
        Some(MediaType.APPLICATION_VND_ETSI_ASIC_E_PLUS_ZIP)
      case "application/vnd.fdf" => Some(MediaType.APPLICATION_VND_FDF)
      case "application/vnd.fdsn.mseed" =>
        Some(MediaType.APPLICATION_VND_FDSN_MSEED)
      case "application/vnd.fdsn.seed" =>
        Some(MediaType.APPLICATION_VND_FDSN_SEED)
      case "application/vnd.framemaker" =>
        Some(MediaType.APPLICATION_VND_FRAMEMAKER)
      case "application/vnd.geogebra.file" =>
        Some(MediaType.APPLICATION_VND_GEOGEBRA_FILE)
      case "application/vnd.geogebra.tool" =>
        Some(MediaType.APPLICATION_VND_GEOGEBRA_TOOL)
      case "application/vnd.geometry-explorer" =>
        Some(MediaType.APPLICATION_VND_GEOMETRY_EXPLORER)
      case "application/vnd.geoplan" =>
        Some(MediaType.APPLICATION_VND_GEOPLAN)
      case "application/vnd.geospace" =>
        Some(MediaType.APPLICATION_VND_GEOSPACE)
      case "application/vnd.google-earth.kml+xml" =>
        Some(MediaType.APPLICATION_VND_GOOGLE_EARTH_KML_PLUS_XML)
      case "application/vnd.google-earth.kmz" =>
        Some(MediaType.APPLICATION_VND_GOOGLE_EARTH_KMZ)
      case "application/vnd.grafeq" =>
        Some(MediaType.APPLICATION_VND_GRAFEQ)
      case "application/vnd.hal+xml" =>
        Some(MediaType.APPLICATION_VND_HAL_PLUS_XML)
      case "application/vnd.hbci" => Some(MediaType.APPLICATION_VND_HBCI)
      case "application/vnd.hp-hpid" =>
        Some(MediaType.APPLICATION_VND_HP_HPID)
      case "application/vnd.ibm.rights-management" =>
        Some(MediaType.APPLICATION_VND_IBM_RIGHTS_MANAGEMENT)
      case "application/vnd.iccprofile" =>
        Some(MediaType.APPLICATION_VND_ICCPROFILE)
      case "application/vnd.intercon.formnet" =>
        Some(MediaType.APPLICATION_VND_INTERCON_FORMNET)
      case "application/vnd.intergeo" =>
        Some(MediaType.APPLICATION_VND_INTERGEO)
      case "application/vnd.joost.joda-archive" =>
        Some(MediaType.APPLICATION_VND_JOOST_JODA_ARCHIVE)
      case "application/vnd.kahootz" =>
        Some(MediaType.APPLICATION_VND_KAHOOTZ)
      case "application/vnd.kde.kpresenter" =>
        Some(MediaType.APPLICATION_VND_KDE_KPRESENTER)
      case "application/vnd.kde.kword" =>
        Some(MediaType.APPLICATION_VND_KDE_KWORD)
      case "application/vnd.Kinar" => Some(MediaType.APPLICATION_VND_KINAR)
      case "application/vnd.koan"  => Some(MediaType.APPLICATION_VND_KOAN)
      case "application/vnd.logipipe.circuit+zip" =>
        Some(MediaType.APPLICATION_VND_LOGIPIPE_CIRCUIT_PLUS_ZIP)
      case "application/vnd.lotus-1-2-3" =>
        Some(MediaType.APPLICATION_VND_LOTUS_1_2_3)
      case "application/vnd.lotus-approach" =>
        Some(MediaType.APPLICATION_VND_LOTUS_APPROACH)
      case "application/vnd.lotus-freelance" =>
        Some(MediaType.APPLICATION_VND_LOTUS_FREELANCE)
      case "application/vnd.lotus-notes" =>
        Some(MediaType.APPLICATION_VND_LOTUS_NOTES)
      case "application/vnd.lotus-organizer" =>
        Some(MediaType.APPLICATION_VND_LOTUS_ORGANIZER)
      case "application/vnd.lotus-wordpro" =>
        Some(MediaType.APPLICATION_VND_LOTUS_WORDPRO)
      case "application/vnd.mif" => Some(MediaType.APPLICATION_VND_MIF)
      case "application/vnd.ms-excel" =>
        Some(MediaType.APPLICATION_VND_MS_EXCEL)
      case "application/vnd.ms-powerpoint" =>
        Some(MediaType.APPLICATION_VND_MS_POWERPOINT)
      case "application/vnd.ms-project" =>
        Some(MediaType.APPLICATION_VND_MS_PROJECT)
      case "application/vnd.ms-tnef" =>
        Some(MediaType.APPLICATION_VND_MS_TNEF)
      case "application/vnd.ms-works" =>
        Some(MediaType.APPLICATION_VND_MS_WORKS)
      case "application/vnd.ms-wpl" =>
        Some(MediaType.APPLICATION_VND_MS_WPL)
      case "application/vnd.mseq" => Some(MediaType.APPLICATION_VND_MSEQ)
      case "application/vnd.musician" =>
        Some(MediaType.APPLICATION_VND_MUSICIAN)
      case "application/vnd.nebumind.line" =>
        Some(MediaType.APPLICATION_VND_NEBUMIND_LINE)
      case "application/vnd.nervana" =>
        Some(MediaType.APPLICATION_VND_NERVANA)
      case "application/vnd.nintendo.snes.rom" =>
        Some(MediaType.APPLICATION_VND_NINTENDO_SNES_ROM)
      case "application/vnd.oasis.opendocument.chart" =>
        Some(MediaType.APPLICATION_VND_OASIS_OPENDOCUMENT_CHART)
      case "application/vnd.oasis.opendocument.graphics" =>
        Some(MediaType.APPLICATION_VND_OASIS_OPENDOCUMENT_GRAPHICS)
      case "application/vnd.oasis.opendocument.image" =>
        Some(MediaType.APPLICATION_VND_OASIS_OPENDOCUMENT_IMAGE)
      case "application/vnd.oasis.opendocument.presentation" =>
        Some(MediaType.APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION)
      case "application/vnd.oasis.opendocument.spreadsheet" =>
        Some(MediaType.APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET)
      case "application/vnd.oasis.opendocument.text" =>
        Some(MediaType.APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT)
      case "application/vnd.oasis.opendocument.text-web" =>
        Some(MediaType.APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_WEB)
      case "application/vnd.openxmlformats-officedocument.presentationml.presentation" =>
        Some(
          MediaType.APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION
        )
      case "application/vnd.openxmlformats-officedocument.presentationml.slide" =>
        Some(
          MediaType.APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDE
        )
      case "application/vnd.openxmlformats-officedocument.presentationml.slideshow" =>
        Some(
          MediaType.APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDESHOW
        )
      case "application/vnd.openxmlformats-officedocument.presentationml.template" =>
        Some(
          MediaType.APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_TEMPLATE
        )
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" =>
        Some(
          MediaType.APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET
        )
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.template" =>
        Some(
          MediaType.APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_TEMPLATE
        )
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.document" =>
        Some(
          MediaType.APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT
        )
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.template" =>
        Some(
          MediaType.APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_TEMPLATE
        )
      case "application/vnd.osgeo.mapguide.package" =>
        Some(MediaType.APPLICATION_VND_OSGEO_MAPGUIDE_PACKAGE)
      case "application/vnd.palm" => Some(MediaType.APPLICATION_VND_PALM)
      case "application/vnd.previewsystems.box" =>
        Some(MediaType.APPLICATION_VND_PREVIEWSYSTEMS_BOX)
      case "application/vnd.Quark.QuarkXPress" =>
        Some(MediaType.APPLICATION_VND_QUARK_QUARKXPRESS)
      case "application/vnd.quobject-quoxdocument" =>
        Some(MediaType.APPLICATION_VND_QUOBJECT_QUOXDOCUMENT)
      case "application/vnd.recordare.musicxml" =>
        Some(MediaType.APPLICATION_VND_RECORDARE_MUSICXML)
      case "application/vnd.recordare.musicxml+xml" =>
        Some(MediaType.APPLICATION_VND_RECORDARE_MUSICXML_PLUS_XML)
      case "application/vnd.resilient.logic" =>
        Some(MediaType.APPLICATION_VND_RESILIENT_LOGIC)
      case "application/vnd.rn-realmedia" =>
        Some(MediaType.APPLICATION_VND_RN_REALMEDIA)
      case "application/vnd.scribus" =>
        Some(MediaType.APPLICATION_VND_SCRIBUS)
      case "application/vnd.sealed.doc" =>
        Some(MediaType.APPLICATION_VND_SEALED_DOC)
      case "application/vnd.sealed.eml" =>
        Some(MediaType.APPLICATION_VND_SEALED_EML)
      case "application/vnd.sealed.mht" =>
        Some(MediaType.APPLICATION_VND_SEALED_MHT)
      case "application/vnd.sealed.ppt" =>
        Some(MediaType.APPLICATION_VND_SEALED_PPT)
      case "application/vnd.sealed.xls" =>
        Some(MediaType.APPLICATION_VND_SEALED_XLS)
      case "application/vnd.sealedmedia.softseal.html" =>
        Some(MediaType.APPLICATION_VND_SEALEDMEDIA_SOFTSEAL_HTML)
      case "application/vnd.sealedmedia.softseal.pdf" =>
        Some(MediaType.APPLICATION_VND_SEALEDMEDIA_SOFTSEAL_PDF)
      case "application/vnd.SimTech-MindMapper" =>
        Some(MediaType.APPLICATION_VND_SIMTECH_MINDMAPPER)
      case "application/vnd.smaf" => Some(MediaType.APPLICATION_VND_SMAF)
      case "application/vnd.snesdev-page-table" =>
        Some(MediaType.APPLICATION_VND_SNESDEV_PAGE_TABLE)
      case "application/vnd.solent.sdkm+xml" =>
        Some(MediaType.APPLICATION_VND_SOLENT_SDKM_PLUS_XML)
      case "application/vnd.sqlite3" =>
        Some(MediaType.APPLICATION_VND_SQLITE3)
      case "application/vnd.stardivision.calc" =>
        Some(MediaType.APPLICATION_VND_STARDIVISION_CALC)
      case "application/vnd.stardivision.draw" =>
        Some(MediaType.APPLICATION_VND_STARDIVISION_DRAW)
      case "application/vnd.stardivision.impress" =>
        Some(MediaType.APPLICATION_VND_STARDIVISION_IMPRESS)
      case "application/vnd.stardivision.math" =>
        Some(MediaType.APPLICATION_VND_STARDIVISION_MATH)
      case "application/vnd.stardivision.writer" =>
        Some(MediaType.APPLICATION_VND_STARDIVISION_WRITER)
      case "application/vnd.stardivision.writer-global" =>
        Some(MediaType.APPLICATION_VND_STARDIVISION_WRITER_GLOBAL)
      case "application/vnd.sun.xml.calc" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_CALC)
      case "application/vnd.sun.xml.calc.template" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_CALC_TEMPLATE)
      case "application/vnd.sun.xml.draw" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_DRAW)
      case "application/vnd.sun.xml.draw.template" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_DRAW_TEMPLATE)
      case "application/vnd.sun.xml.impress" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_IMPRESS)
      case "application/vnd.sun.xml.impress.template" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_IMPRESS_TEMPLATE)
      case "application/vnd.sun.xml.math" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_MATH)
      case "application/vnd.sun.xml.writer" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_WRITER)
      case "application/vnd.sun.xml.writer.global" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_WRITER_GLOBAL)
      case "application/vnd.sun.xml.writer.template" =>
        Some(MediaType.APPLICATION_VND_SUN_XML_WRITER_TEMPLATE)
      case "application/vnd.sus-calendar" =>
        Some(MediaType.APPLICATION_VND_SUS_CALENDAR)
      case "application/vnd.tcpdump.pcap" =>
        Some(MediaType.APPLICATION_VND_TCPDUMP_PCAP)
      case "application/vnd.tml"   => Some(MediaType.APPLICATION_VND_TML)
      case "application/vnd.ufdl"  => Some(MediaType.APPLICATION_VND_UFDL)
      case "application/vnd.unity" => Some(MediaType.APPLICATION_VND_UNITY)
      case "application/vnd.uoml+xml" =>
        Some(MediaType.APPLICATION_VND_UOML_PLUS_XML)
      case "application/vnd.uri-map" =>
        Some(MediaType.APPLICATION_VND_URI_MAP)
      case "application/vnd.vd-study" =>
        Some(MediaType.APPLICATION_VND_VD_STUDY)
      case "application/vnd.veritone.aion+json" =>
        Some(MediaType.APPLICATION_VND_VERITONE_AION_PLUS_JSON)
      case "application/vnd.veryant.thin" =>
        Some(MediaType.APPLICATION_VND_VERYANT_THIN)
      case "application/vnd.visio" => Some(MediaType.APPLICATION_VND_VISIO)
      case "application/vnd.visio2013" =>
        Some(MediaType.APPLICATION_VND_VISIO2013)
      case "application/vnd.wolfram.player" =>
        Some(MediaType.APPLICATION_VND_WOLFRAM_PLAYER)
      case "application/vnd.wordperfect" =>
        Some(MediaType.APPLICATION_VND_WORDPERFECT)
      case "application/vnd.xfdl" => Some(MediaType.APPLICATION_VND_XFDL)
      case "application/vnd.yamaha.openscoreformat" =>
        Some(MediaType.APPLICATION_VND_YAMAHA_OPENSCOREFORMAT)
      case "application/vnd.yamaha.smaf-audio" =>
        Some(MediaType.APPLICATION_VND_YAMAHA_SMAF_AUDIO)
      case "application/vnd.yamaha.smaf-phrase" =>
        Some(MediaType.APPLICATION_VND_YAMAHA_SMAF_PHRASE)
      case "application/vnd.zul" => Some(MediaType.APPLICATION_VND_ZUL)
      case "application/voicexml+xml" =>
        Some(MediaType.APPLICATION_VOICEXML_PLUS_XML)
      case "application/wasm" => Some(MediaType.APPLICATION_WASM)
      case "application/wspolicy+xml" =>
        Some(MediaType.APPLICATION_WSPOLICY_PLUS_XML)
      case "application/x-7z-compressed" =>
        Some(MediaType.APPLICATION_X_7Z_COMPRESSED)
      case "application/x-abiword" => Some(MediaType.APPLICATION_X_ABIWORD)
      case "application/x-ace-compressed" =>
        Some(MediaType.APPLICATION_X_ACE_COMPRESSED)
      case "application/x-apple-diskimage" =>
        Some(MediaType.APPLICATION_X_APPLE_DISKIMAGE)
      case "application/x-authorware-bin" =>
        Some(MediaType.APPLICATION_X_AUTHORWARE_BIN)
      case "application/x-authorware-map" =>
        Some(MediaType.APPLICATION_X_AUTHORWARE_MAP)
      case "application/x-authorware-seg" =>
        Some(MediaType.APPLICATION_X_AUTHORWARE_SEG)
      case "application/x-bcpio" => Some(MediaType.APPLICATION_X_BCPIO)
      case "application/x-bittorrent" =>
        Some(MediaType.APPLICATION_X_BITTORRENT)
      case "application/x-bzip"  => Some(MediaType.APPLICATION_X_BZIP)
      case "application/x-bzip2" => Some(MediaType.APPLICATION_X_BZIP2)
      case "application/x-cdf"   => Some(MediaType.APPLICATION_X_CDF)
      case "application/x-chat"  => Some(MediaType.APPLICATION_X_CHAT)
      case "application/x-cpio"  => Some(MediaType.APPLICATION_X_CPIO)
      case "application/x-csh"   => Some(MediaType.APPLICATION_X_CSH)
      case "application/x-debian-package" =>
        Some(MediaType.APPLICATION_X_DEBIAN_PACKAGE)
      case "application/x-director" =>
        Some(MediaType.APPLICATION_X_DIRECTOR)
      case "application/x-dtbook+xml" =>
        Some(MediaType.APPLICATION_X_DTBOOK_PLUS_XML)
      case "application/x-dtbresource+xml" =>
        Some(MediaType.APPLICATION_X_DTBRESOURCE_PLUS_XML)
      case "application/x-dvi"  => Some(MediaType.APPLICATION_X_DVI)
      case "application/x-font" => Some(MediaType.APPLICATION_X_FONT)
      case "application/x-font-bdf" =>
        Some(MediaType.APPLICATION_X_FONT_BDF)
      case "application/x-font-ghostscript" =>
        Some(MediaType.APPLICATION_X_FONT_GHOSTSCRIPT)
      case "application/x-font-otf" =>
        Some(MediaType.APPLICATION_X_FONT_OTF)
      case "application/x-font-pcf" =>
        Some(MediaType.APPLICATION_X_FONT_PCF)
      case "application/x-font-ttf" =>
        Some(MediaType.APPLICATION_X_FONT_TTF)
      case "application/x-font-woff" =>
        Some(MediaType.APPLICATION_X_FONT_WOFF)
      case "application/x-gnumeric" =>
        Some(MediaType.APPLICATION_X_GNUMERIC)
      case "application/x-gtar" => Some(MediaType.APPLICATION_X_GTAR)
      case "application/x-gtar-compressed" =>
        Some(MediaType.APPLICATION_X_GTAR_COMPRESSED)
      case "application/x-hdf" => Some(MediaType.APPLICATION_X_HDF)
      case "application/x-internet-signup" =>
        Some(MediaType.APPLICATION_X_INTERNET_SIGNUP)
      case "application/x-koan" => Some(MediaType.APPLICATION_X_KOAN)
      case "application/x-kpresenter" =>
        Some(MediaType.APPLICATION_X_KPRESENTER)
      case "application/x-kword" => Some(MediaType.APPLICATION_X_KWORD)
      case "application/x-latex" => Some(MediaType.APPLICATION_X_LATEX)
      case "application/x-maker" => Some(MediaType.APPLICATION_X_MAKER)
      case "application/x-mobipocket-ebook" =>
        Some(MediaType.APPLICATION_X_MOBIPOCKET_EBOOK)
      case "application/x-msaccess" =>
        Some(MediaType.APPLICATION_X_MSACCESS)
      case "application/x-msdos-program" =>
        Some(MediaType.APPLICATION_X_MSDOS_PROGRAM)
      case "application/x-msdownload" =>
        Some(MediaType.APPLICATION_X_MSDOWNLOAD)
      case "application/x-python-code" =>
        Some(MediaType.APPLICATION_X_PYTHON_CODE)
      case "application/x-qgis" => Some(MediaType.APPLICATION_X_QGIS)
      case "application/x-rar-compressed" =>
        Some(MediaType.APPLICATION_X_RAR_COMPRESSED)
      case "application/x-scilab" => Some(MediaType.APPLICATION_X_SCILAB)
      case "application/x-sh"     => Some(MediaType.APPLICATION_X_SH)
      case "application/x-shar"   => Some(MediaType.APPLICATION_X_SHAR)
      case "application/x-shockwave-flash" =>
        Some(MediaType.APPLICATION_X_SHOCKWAVE_FLASH)
      case "application/x-stuffit" => Some(MediaType.APPLICATION_X_STUFFIT)
      case "application/x-stuffitx" =>
        Some(MediaType.APPLICATION_X_STUFFITX)
      case "application/x-tar"     => Some(MediaType.APPLICATION_X_TAR)
      case "application/x-tex"     => Some(MediaType.APPLICATION_X_TEX)
      case "application/x-texinfo" => Some(MediaType.APPLICATION_X_TEXINFO)
      case "application/x-trash"   => Some(MediaType.APPLICATION_X_TRASH)
      case "application/x-troff"   => Some(MediaType.APPLICATION_X_TROFF)
      case "application/x-xfig"    => Some(MediaType.APPLICATION_X_XFIG)
      case "application/xenc+xml" =>
        Some(MediaType.APPLICATION_XENC_PLUS_XML)
      case "application/xhtml+xml" =>
        Some(MediaType.APPLICATION_XHTML_PLUS_XML)
      case "application/xml"     => Some(MediaType.APPLICATION_XML)
      case "application/xml-dtd" => Some(MediaType.APPLICATION_XML_DTD)
      case "application/xop+xml" => Some(MediaType.APPLICATION_XOP_PLUS_XML)
      case "application/xslt+xml" =>
        Some(MediaType.APPLICATION_XSLT_PLUS_XML)
      case "application/xspf+xml" =>
        Some(MediaType.APPLICATION_XSPF_PLUS_XML)
      case "application/xv+xml"   => Some(MediaType.APPLICATION_XV_PLUS_XML)
      case "application/zip"      => Some(MediaType.APPLICATION_ZIP)
      case "audio/3gp2"           => Some(MediaType.AUDIO_3GP2)
      case "audio/3gpp"           => Some(MediaType.AUDIO_3GPP)
      case "audio/3gpp2"          => Some(MediaType.AUDIO_3GPP2)
      case "audio/aac"            => Some(MediaType.AUDIO_AAC)
      case "audio/AMR"            => Some(MediaType.AUDIO_AMR)
      case "audio/AMR-WB"         => Some(MediaType.AUDIO_AMR_WB)
      case "audio/ATRAC3"         => Some(MediaType.AUDIO_ATRAC3)
      case "audio/basic"          => Some(MediaType.AUDIO_BASIC)
      case "audio/csound"         => Some(MediaType.AUDIO_CSOUND)
      case "audio/EVRC-QCP"       => Some(MediaType.AUDIO_EVRC_QCP)
      case "audio/flac"           => Some(MediaType.AUDIO_FLAC)
      case "audio/midi"           => Some(MediaType.AUDIO_MIDI)
      case "audio/mp4"            => Some(MediaType.AUDIO_MP4)
      case "audio/mpeg"           => Some(MediaType.AUDIO_MPEG)
      case "audio/ogg"            => Some(MediaType.AUDIO_OGG)
      case "audio/prs.sid"        => Some(MediaType.AUDIO_PRS_SID)
      case "audio/usac"           => Some(MediaType.AUDIO_USAC)
      case "audio/vnd.dece.audio" => Some(MediaType.AUDIO_VND_DECE_AUDIO)
      case "audio/vnd.digital-winds" =>
        Some(MediaType.AUDIO_VND_DIGITAL_WINDS)
      case "audio/vnd.dra"    => Some(MediaType.AUDIO_VND_DRA)
      case "audio/vnd.dts"    => Some(MediaType.AUDIO_VND_DTS)
      case "audio/vnd.dts.hd" => Some(MediaType.AUDIO_VND_DTS_HD)
      case "audio/vnd.sealedmedia.softseal.mpeg" =>
        Some(MediaType.AUDIO_VND_SEALEDMEDIA_SOFTSEAL_MPEG)
      case "audio/webm"           => Some(MediaType.AUDIO_WEBM)
      case "audio/x-aac"          => Some(MediaType.AUDIO_X_AAC)
      case "audio/x-aiff"         => Some(MediaType.AUDIO_X_AIFF)
      case "audio/x-ms-wax"       => Some(MediaType.AUDIO_X_MS_WAX)
      case "audio/x-ms-wma"       => Some(MediaType.AUDIO_X_MS_WMA)
      case "audio/x-pn-realaudio" => Some(MediaType.AUDIO_X_PN_REALAUDIO)
      case "audio/x-wav"          => Some(MediaType.AUDIO_X_WAV)
      case "chemical/x-cache"     => Some(MediaType.CHEMICAL_X_CACHE)
      case "chemical/x-cactvs-binary" =>
        Some(MediaType.CHEMICAL_X_CACTVS_BINARY)
      case "chemical/x-cdx"  => Some(MediaType.CHEMICAL_X_CDX)
      case "chemical/x-cif"  => Some(MediaType.CHEMICAL_X_CIF)
      case "chemical/x-cmdf" => Some(MediaType.CHEMICAL_X_CMDF)
      case "chemical/x-cml"  => Some(MediaType.CHEMICAL_X_CML)
      case "chemical/x-csml" => Some(MediaType.CHEMICAL_X_CSML)
      case "chemical/x-cxf"  => Some(MediaType.CHEMICAL_X_CXF)
      case "chemical/x-embl-dl-nucleotide" =>
        Some(MediaType.CHEMICAL_X_EMBL_DL_NUCLEOTIDE)
      case "chemical/x-gamess-input" =>
        Some(MediaType.CHEMICAL_X_GAMESS_INPUT)
      case "chemical/x-gaussian-checkpoint" =>
        Some(MediaType.CHEMICAL_X_GAUSSIAN_CHECKPOINT)
      case "chemical/x-gaussian-input" =>
        Some(MediaType.CHEMICAL_X_GAUSSIAN_INPUT)
      case "chemical/x-isostar"  => Some(MediaType.CHEMICAL_X_ISOSTAR)
      case "chemical/x-jcamp-dx" => Some(MediaType.CHEMICAL_X_JCAMP_DX)
      case "chemical/x-macromodel-input" =>
        Some(MediaType.CHEMICAL_X_MACROMODEL_INPUT)
      case "chemical/x-mdl-sdfile" => Some(MediaType.CHEMICAL_X_MDL_SDFILE)
      case "chemical/x-mopac-input" =>
        Some(MediaType.CHEMICAL_X_MOPAC_INPUT)
      case "chemical/x-ncbi-asn1-ascii" =>
        Some(MediaType.CHEMICAL_X_NCBI_ASN1_ASCII)
      case "chemical/x-ncbi-asn1-binary" =>
        Some(MediaType.CHEMICAL_X_NCBI_ASN1_BINARY)
      case "chemical/x-pdb" => Some(MediaType.CHEMICAL_X_PDB)
      case "font/otf"       => Some(MediaType.FONT_OTF)
      case "font/sfnt"      => Some(MediaType.FONT_SFNT)
      case "font/ttf"       => Some(MediaType.FONT_TTF)
      case "image/avif"     => Some(MediaType.IMAGE_AVIF)
      case "image/bmp"      => Some(MediaType.IMAGE_BMP)
      case "image/cgm"      => Some(MediaType.IMAGE_CGM)
      case "image/fits"     => Some(MediaType.IMAGE_FITS)
      case "image/g3fax"    => Some(MediaType.IMAGE_G3FAX)
      case "image/gif"      => Some(MediaType.IMAGE_GIF)
      case "image/ief"      => Some(MediaType.IMAGE_IEF)
      case "image/jp2"      => Some(MediaType.IMAGE_JP2)
      case "image/jpeg"     => Some(MediaType.IMAGE_JPEG)
      case "image/jphc"     => Some(MediaType.IMAGE_JPHC)
      case "image/jpm"      => Some(MediaType.IMAGE_JPM)
      case "image/jpx"      => Some(MediaType.IMAGE_JPX)
      case "image/ktx"      => Some(MediaType.IMAGE_KTX)
      case "image/pjpeg"    => Some(MediaType.IMAGE_PJPEG)
      case "image/png"      => Some(MediaType.IMAGE_PNG)
      case "image/prs.btif" => Some(MediaType.IMAGE_PRS_BTIF)
      case "image/svg+xml"  => Some(MediaType.IMAGE_SVG_PLUS_XML)
      case "image/tiff"     => Some(MediaType.IMAGE_TIFF)
      case "image/vnd.adobe.photoshop" =>
        Some(MediaType.IMAGE_VND_ADOBE_PHOTOSHOP)
      case "image/vnd.dece.graphic" =>
        Some(MediaType.IMAGE_VND_DECE_GRAPHIC)
      case "image/vnd.djvu" => Some(MediaType.IMAGE_VND_DJVU)
      case "image/vnd.dwg"  => Some(MediaType.IMAGE_VND_DWG)
      case "image/vnd.dxf"  => Some(MediaType.IMAGE_VND_DXF)
      case "image/vnd.fpx"  => Some(MediaType.IMAGE_VND_FPX)
      case "image/vnd.globalgraphics.pgb" =>
        Some(MediaType.IMAGE_VND_GLOBALGRAPHICS_PGB)
      case "image/vnd.ms-modi"    => Some(MediaType.IMAGE_VND_MS_MODI)
      case "image/vnd.net-fpx"    => Some(MediaType.IMAGE_VND_NET_FPX)
      case "image/vnd.radiance"   => Some(MediaType.IMAGE_VND_RADIANCE)
      case "image/vnd.sealed.png" => Some(MediaType.IMAGE_VND_SEALED_PNG)
      case "image/vnd.sealedmedia.softseal.gif" =>
        Some(MediaType.IMAGE_VND_SEALEDMEDIA_SOFTSEAL_GIF)
      case "image/vnd.sealedmedia.softseal.jpg" =>
        Some(MediaType.IMAGE_VND_SEALEDMEDIA_SOFTSEAL_JPG)
      case "image/vnd.xiff"      => Some(MediaType.IMAGE_VND_XIFF)
      case "image/webp"          => Some(MediaType.IMAGE_WEBP)
      case "image/x-citrix-jpeg" => Some(MediaType.IMAGE_X_CITRIX_JPEG)
      case "image/x-cmu-raster"  => Some(MediaType.IMAGE_X_CMU_RASTER)
      case "image/x-cmx"         => Some(MediaType.IMAGE_X_CMX)
      case "image/x-freehand"    => Some(MediaType.IMAGE_X_FREEHAND)
      case "image/x-icon"        => Some(MediaType.IMAGE_X_ICON)
      case "image/x-pcx"         => Some(MediaType.IMAGE_X_PCX)
      case "image/x-pict"        => Some(MediaType.IMAGE_X_PICT)
      case "image/x-portable-anymap" =>
        Some(MediaType.IMAGE_X_PORTABLE_ANYMAP)
      case "image/x-portable-bitmap" =>
        Some(MediaType.IMAGE_X_PORTABLE_BITMAP)
      case "image/x-portable-graymap" =>
        Some(MediaType.IMAGE_X_PORTABLE_GRAYMAP)
      case "image/x-rgb"     => Some(MediaType.IMAGE_X_RGB)
      case "image/x-xbitmap" => Some(MediaType.IMAGE_X_XBITMAP)
      case "image/x-xpixmap" => Some(MediaType.IMAGE_X_XPIXMAP)
      case "message/rfc822"  => Some(MediaType.MESSAGE_RFC822)
      case "model/iges"      => Some(MediaType.MODEL_IGES)
      case "model/mesh"      => Some(MediaType.MODEL_MESH)
      case "model/step"      => Some(MediaType.MODEL_STEP)
      case "model/vnd.collada+xml" =>
        Some(MediaType.MODEL_VND_COLLADA_PLUS_XML)
      case "model/vnd.dwf" => Some(MediaType.MODEL_VND_DWF)
      case "model/vnd.gdl" => Some(MediaType.MODEL_VND_GDL)
      case "model/vnd.gtw" => Some(MediaType.MODEL_VND_GTW)
      case "model/vnd.parasolid.transmit.binary" =>
        Some(MediaType.MODEL_VND_PARASOLID_TRANSMIT_BINARY)
      case "model/vnd.parasolid.transmit.text" =>
        Some(MediaType.MODEL_VND_PARASOLID_TRANSMIT_TEXT)
      case "model/vrml"          => Some(MediaType.MODEL_VRML)
      case "model/x3d+xml"       => Some(MediaType.MODEL_X3D_PLUS_XML)
      case "model/x3d-vrml"      => Some(MediaType.MODEL_X3D_VRML)
      case "text/cache-manifest" => Some(MediaType.TEXT_CACHE_MANIFEST)
      case "text/calendar"       => Some(MediaType.TEXT_CALENDAR)
      case "text/css"            => Some(MediaType.TEXT_CSS)
      case "text/csv"            => Some(MediaType.TEXT_CSV)
      case "text/dns"            => Some(MediaType.TEXT_DNS)
      case "text/html"           => Some(MediaType.TEXT_HTML)
      case "text/javascript"     => Some(MediaType.TEXT_JAVASCRIPT)
      case "text/markdown"       => Some(MediaType.TEXT_MARKDOWN)
      case "text/plain"          => Some(MediaType.TEXT_PLAIN)
      case "text/prs.lines.tag"  => Some(MediaType.TEXT_PRS_LINES_TAG)
      case "text/richtext"       => Some(MediaType.TEXT_RICHTEXT)
      case "text/scriptlet"      => Some(MediaType.TEXT_SCRIPTLET)
      case "text/sgml"           => Some(MediaType.TEXT_SGML)
      case "text/shaclc"         => Some(MediaType.TEXT_SHACLC)
      case "text/tab-separated-values" =>
        Some(MediaType.TEXT_TAB_SEPARATED_VALUES)
      case "text/troff"            => Some(MediaType.TEXT_TROFF)
      case "text/turtle"           => Some(MediaType.TEXT_TURTLE)
      case "text/uri-list"         => Some(MediaType.TEXT_URI_LIST)
      case "text/vcard"            => Some(MediaType.TEXT_VCARD)
      case "text/vnd.graphviz"     => Some(MediaType.TEXT_VND_GRAPHVIZ)
      case "text/vnd.in3d.3dml"    => Some(MediaType.TEXT_VND_IN3D_3DML)
      case "text/vnd.in3d.spot"    => Some(MediaType.TEXT_VND_IN3D_SPOT)
      case "text/vtt"              => Some(MediaType.TEXT_VTT)
      case "text/x-c++hdr"         => Some(MediaType.TEXT_X_C_PLUS__PLUS_HDR)
      case "text/x-c++src"         => Some(MediaType.TEXT_X_C_PLUS__PLUS_SRC)
      case "text/x-diff"           => Some(MediaType.TEXT_X_DIFF)
      case "text/x-java-source"    => Some(MediaType.TEXT_X_JAVA_SOURCE)
      case "text/x-pascal"         => Some(MediaType.TEXT_X_PASCAL)
      case "text/x-perl"           => Some(MediaType.TEXT_X_PERL)
      case "text/x-tcl"            => Some(MediaType.TEXT_X_TCL)
      case "text/x-tex"            => Some(MediaType.TEXT_X_TEX)
      case "text/x-uuencode"       => Some(MediaType.TEXT_X_UUENCODE)
      case "text/x-vcalendar"      => Some(MediaType.TEXT_X_VCALENDAR)
      case "text/x-vcard"          => Some(MediaType.TEXT_X_VCARD)
      case "text/xml"              => Some(MediaType.TEXT_XML)
      case "text/yaml"             => Some(MediaType.TEXT_YAML)
      case "video/3gpp"            => Some(MediaType.VIDEO_3GPP)
      case "video/3gpp2"           => Some(MediaType.VIDEO_3GPP2)
      case "video/dv"              => Some(MediaType.VIDEO_DV)
      case "video/h261"            => Some(MediaType.VIDEO_H261)
      case "video/h263"            => Some(MediaType.VIDEO_H263)
      case "video/h264"            => Some(MediaType.VIDEO_H264)
      case "video/jpeg"            => Some(MediaType.VIDEO_JPEG)
      case "video/jpm"             => Some(MediaType.VIDEO_JPM)
      case "video/mj2"             => Some(MediaType.VIDEO_MJ2)
      case "video/mp4"             => Some(MediaType.VIDEO_MP4)
      case "video/mpeg"            => Some(MediaType.VIDEO_MPEG)
      case "video/ogg"             => Some(MediaType.VIDEO_OGG)
      case "video/quicktime"       => Some(MediaType.VIDEO_QUICKTIME)
      case "video/vnd.dece.hd"     => Some(MediaType.VIDEO_VND_DECE_HD)
      case "video/vnd.dece.mobile" => Some(MediaType.VIDEO_VND_DECE_MOBILE)
      case "video/vnd.dece.mp4"    => Some(MediaType.VIDEO_VND_DECE_MP4)
      case "video/vnd.dece.pd"     => Some(MediaType.VIDEO_VND_DECE_PD)
      case "video/vnd.dece.sd"     => Some(MediaType.VIDEO_VND_DECE_SD)
      case "video/vnd.dece.video"  => Some(MediaType.VIDEO_VND_DECE_VIDEO)
      case "video/vnd.mpegurl"     => Some(MediaType.VIDEO_VND_MPEGURL)
      case "video/vnd.radgamettools.bink" =>
        Some(MediaType.VIDEO_VND_RADGAMETTOOLS_BINK)
      case "video/vnd.sealed.mpeg1" =>
        Some(MediaType.VIDEO_VND_SEALED_MPEG1)
      case "video/vnd.sealed.swf" => Some(MediaType.VIDEO_VND_SEALED_SWF)
      case "video/vnd.sealedmedia.softseal.mov" =>
        Some(MediaType.VIDEO_VND_SEALEDMEDIA_SOFTSEAL_MOV)
      case "video/vnd.uvvu.mp4" => Some(MediaType.VIDEO_VND_UVVU_MP4)
      case "video/webm"         => Some(MediaType.VIDEO_WEBM)
      case "video/x-f4v"        => Some(MediaType.VIDEO_X_F4V)
      case "video/x-fli"        => Some(MediaType.VIDEO_X_FLI)
      case "video/x-flv"        => Some(MediaType.VIDEO_X_FLV)
      case "video/x-la-asf"     => Some(MediaType.VIDEO_X_LA_ASF)
      case "video/x-m4v"        => Some(MediaType.VIDEO_X_M4V)
      case "video/x-matroska"   => Some(MediaType.VIDEO_X_MATROSKA)
      case "video/x-ms-asf"     => Some(MediaType.VIDEO_X_MS_ASF)
      case "video/x-ms-wm"      => Some(MediaType.VIDEO_X_MS_WM)
      case "video/x-ms-wmv"     => Some(MediaType.VIDEO_X_MS_WMV)
      case "video/x-ms-wmx"     => Some(MediaType.VIDEO_X_MS_WMX)
      case "video/x-ms-wvx"     => Some(MediaType.VIDEO_X_MS_WVX)
      case "video/x-msvideo"    => Some(MediaType.VIDEO_X_MSVIDEO)
      case "video/x-sgi-movie"  => Some(MediaType.VIDEO_X_SGI_MOVIE)
      case "x-world/x-vrml"     => Some(MediaType.X_WORLD_X_VRML)
      case _                    => None
    }
  //scalastyle:on

}
