/*
 * IIIF Manifest Creator
 * Copyright (C) 2021  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

object ManifestUtils {

  val manifestId: String => String => String = digitalObjectId =>
    version => s"$manifestUriPlaceholder/$digitalObjectId/manifest/$version"

  private val dateTimeFormatter = DateTimeFormatter.ISO_DATE

  private lazy val onlyYear = "^(\\d{4})$".r // e.g. 1931
  private lazy val onlyMonth = "^(\\d{4}-\\d{2})$".r // e.g. 1931-12
  private lazy val date = "^(\\d{4}-\\d{2}-\\d{2})$".r // e.g. 1931-12-30
  private lazy val yearRange = "^(\\d{4}/\\d{4})$".r // e.g. 1931/1964
  private lazy val monthRange =
    "^(\\d{4}-\\d{2}/\\d{4}-\\d{2})$".r // e.g. 1931-12/1964-01
  private lazy val dayRange =
    "^(\\d{4}-\\d{2}-\\d{2}/\\d{4}-\\d{2}-\\d{2})$".r // e.g. 1931-12-30/1964-01-25
  private lazy val monthInYearRange =
    "^(\\d{4}-\\d{2}/\\d{2})$".r // e.g. 1931-01/12
  private lazy val dayInMonthRange =
    "^(\\d{4}-\\d{2}-\\d{2}/\\d{2})$".r // e.g. 1931-12-01/30
  private lazy val dayInYearRange =
    "^(\\d{4}-\\d{2}-\\d{2}/\\d{2}-\\d{2})$".r // e.g. 1931-01-25/12-30
  private lazy val untilYear = "^(\\?/\\d{4})$".r // e.g. ?/1964
  private lazy val fromYear = "^(\\d{4}/\\?)$".r // e.g. 1931/?

  private def parseTimestamp(parseDate: => LocalDate): DateExtractionResult = {
    Try(parseDate) match {
      case Success(v) => DateExtractionResult(Some(v))
      case Failure(ex) =>
        DateExtractionResult(None, warnings = Some(ex.getMessage))
    }
  }

  // scalastyle:off cyclomatic.complexity method.length
  def createTimestamp(value: String): DateExtractionResult = {
    value match {
      case date(value) =>
        parseTimestamp(LocalDate.parse(value, dateTimeFormatter))
      case onlyYear(value) =>
        parseTimestamp(LocalDate.parse(s"$value-01-01", dateTimeFormatter))
      case onlyMonth(value) =>
        parseTimestamp(LocalDate.parse(s"$value-01", dateTimeFormatter))
      case yearRange(value) =>
        val dateRangeStart = value.split("/")(0)
        val dateRangeEnd = value.split("/")(1)
        parseTimestamp(
          LocalDate.parse(s"${value.split("/")(0)}-01-01", dateTimeFormatter)
        ) match {
          case der @ DateExtractionResult(Some(_), _, None) =>
            der.copy(metadata =
              List(
                ("Date range start", dateRangeStart),
                ("Date range end", dateRangeEnd)
              )
            )
          case der @ _ => der
        }
      case monthRange(value) =>
        val dateRangeStart = value.split("/")(0)
        val dateRangeEnd = value.split("/")(1)
        parseTimestamp(
          LocalDate.parse(s"$dateRangeStart-01", dateTimeFormatter)
        ) match {
          case der @ DateExtractionResult(Some(_), _, None) =>
            der.copy(metadata =
              List(
                ("Date range start", dateRangeStart),
                ("Date range end", dateRangeEnd)
              )
            )
          case der @ _ => der
        }
      case dayRange(value) =>
        val dateRangeStart = value.split("/")(0)
        val dateRangeEnd = value.split("/")(1)
        parseTimestamp(
          LocalDate.parse(dateRangeStart, dateTimeFormatter)
        ) match {
          case der @ DateExtractionResult(Some(_), _, None) =>
            der.copy(metadata =
              List(
                ("Date range start", dateRangeStart),
                ("Date range end", dateRangeEnd)
              )
            )
          case der @ _ => der
        }
      case monthInYearRange(value) =>
        val dateRangeStart = value.split("/")(0)
        val dateRangeEnd = value.split("/")(1)
        parseTimestamp(
          LocalDate.parse(s"$dateRangeStart-01", dateTimeFormatter)
        ) match {
          case der @ DateExtractionResult(Some(_), _, None) =>
            der.copy(metadata =
              List(
                ("Date range start", dateRangeStart),
                ("Date range end", dateRangeEnd)
              )
            )
          case der @ _ => der
        }
      case dayInMonthRange(value) =>
        val dateRangeStart = value.split("/")(0)
        val dateRangeEnd = value.split("/")(1)
        parseTimestamp(
          LocalDate.parse(dateRangeStart, dateTimeFormatter)
        ) match {
          case der @ DateExtractionResult(Some(_), _, None) =>
            der.copy(metadata =
              List(
                ("Date range start", dateRangeStart),
                (
                  "Date range end",
                  dateRangeStart.substring(0, 8) + dateRangeEnd
                )
              )
            )
          case der @ _ => der
        }
      case dayInYearRange(value) =>
        val dateRangeStart = value.split("/")(0)
        val dateRangeEnd = value.split("/")(1)
        parseTimestamp(
          LocalDate.parse(dateRangeStart, dateTimeFormatter)
        ) match {
          case der @ DateExtractionResult(Some(_), _, None) =>
            der.copy(metadata =
              List(
                ("Date range start", dateRangeStart),
                (
                  "Date range end",
                  dateRangeStart.substring(0, 5) + dateRangeEnd
                )
              )
            )
          case der @ _ => der
        }
      case fromYear(value) =>
        val dateRangeStart = value.split("/")(0)
        parseTimestamp(
          LocalDate.parse(s"$dateRangeStart-01-01", dateTimeFormatter)
        ) match {
          case der @ DateExtractionResult(Some(_), _, None) =>
            der.copy(metadata =
              List(
                ("Date range start", dateRangeStart),
                ("Date range end", "(unknown)")
              )
            )
          case der @ _ => der
        }
      case untilYear(value) =>
        parseTimestamp(
          LocalDate.parse(s"${value.split("/")(1)}-01-01", dateTimeFormatter)
        ) match {
          case der @ DateExtractionResult(Some(_), _, None) =>
            der.copy(metadata =
              List(
                ("Date range start", "(unknown)"),
                ("Date range end", "(unknown)")
              )
            )
          case der @ _ => der

        }
      case value =>
        DateExtractionResult(
          None,
          warnings = Some(s"Date ($value) has incorrect value")
        )
    }
  }
  // scalastyle:on

  private lazy val hoursMinutesSeconds = "^(\\d{1,}:\\d{2}:\\d{2})$".r
  private lazy val minutesSeconds = "^(\\d{1,}:\\d{2})$".r
  private lazy val seconds = "^(\\d+.\\d+)$".r

  def parseDuration(value: String): DurationExtractionResult =
    value match {
      case hoursMinutesSeconds(v) =>
        val duration = v.split(":")
        Try {
          duration(0).toDouble * 3600 + duration(1).toDouble * 60 + duration(
            2
          ).toDouble
        } match {
          case Success(v) => DurationExtractionSuccess(v)
          case Failure(err) =>
            DurationExtractionWarning(err.getMessage)
        }
      case minutesSeconds(v) =>
        val duration = v.split(":")
        Try {
          duration(0).toDouble * 60 + duration(1).toDouble
        } match {
          case Success(v) => DurationExtractionSuccess(v)
          case Failure(err) =>
            DurationExtractionWarning(err.getMessage)
        }
      case seconds(v) =>
        Try { v.toDouble } match {
          case Success(v) => DurationExtractionSuccess(v)
          case Failure(err) =>
            DurationExtractionWarning(err.getMessage)
        }
      case v =>
        DurationExtractionWarning(s"Duration format `$v` not recognised")
    }

  val mediaUriPlaceholder: String = "http://media.uri.placeholder"
  val manifestUriPlaceholder: String = "http://manifest.uri.placeholder"
  val renderingUriPlaceholder: String = "http://rendering.uri.placeholder"

  def replaceDomainNamesWithPlaceholders(jsonObj: String): String = {
    jsonObj
      .replaceAll(mediaUriPlaceholder, "{{.IIIFImage}}")
      .replaceAll(manifestUriPlaceholder, "{{.IIIFManifest}}")
      .replaceAll(renderingUriPlaceholder, "{{.IIIFRendering}}")
      .replaceAll("(\\\\r|\\\\n|\\\\r\\\\n)", raw"\\\\n")
  }

}

case class ExtractionResult[T](
    obj: T,
    warnings: mutable.Buffer[String] = mutable.Buffer()
)

case class DateExtractionResult(
    date: Option[LocalDate],
    metadata: List[(String, String)] = List(),
    warnings: Option[String] = None
)

sealed trait DurationExtractionResult
case class DurationExtractionSuccess(value: Double)
    extends DurationExtractionResult
case class DurationExtractionWarning(warning: String)
    extends DurationExtractionResult
